#pragma once
#include <Windows.h>
#include <string>
#include <iostream>
using std::string;
#ifndef OUT
#define OUT
#endif



inline bool ReadFileContent(const char* pFilePath, const char* pReadMode, OUT string& strContent)
{
	strContent.clear();
	FILE* fp = NULL;
	fopen_s(&fp, pFilePath, pReadMode);
	if (NULL == fp)
	{
		std::cout<<"文件："<<pFilePath<<" 不存在！"<<std::endl;
		return false;
	}
	//获取文件大小，申请内存空间读取二进制信息
	fseek(fp, 0, SEEK_END);//移动文件指针到文件末尾
	long nSize = ftell(fp);//获取文件指针位置即为文件大小
	fseek(fp, 0, SEEK_SET);//文件指针移动到文件开始
	unsigned char* pBuffer = (unsigned char*)malloc(nSize);
	memset(pBuffer, 0, nSize);
	size_t nCount = fread(pBuffer, 1, nSize, fp);
	fclose(fp);
	strContent.assign((char*)pBuffer, nCount);
	free(pBuffer);
	return true;
}

//获取exe运行路径 最后面带
inline string GetRunPathA()
{
	char szPath[MAX_PATH] = {0};
	GetModuleFileNameA(NULL, szPath, MAX_PATH);
	string strPath(szPath);
	size_t nPos = strPath.find_last_of("\\");
	strPath = strPath.substr(0, nPos+1);
	return strPath;
}