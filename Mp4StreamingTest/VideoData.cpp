#pragma once
#include "VideoData.h"
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>


VideoData* videodata_new(int length)
{
	VideoData* ret = (VideoData*)malloc(sizeof(VideoData));
	ret->length = length;
	ret->data = (unsigned char*)malloc(length);
	return ret;
}

void videodata_delete(VideoData* vd){
	free(vd->data);
	free(vd);
}

void videodata_copy(VideoData* dst, unsigned char* src, int len){
	memcpy(dst->data, src, len);
	dst->length = len;
}
