#pragma once
#include <stdio.h>
#include <tchar.h>

typedef struct _videoData VideoData;

struct _videoData{
	unsigned char* data;
	int length;
};

VideoData* videodata_new(int length);
void videodata_delete(VideoData* vd);
void videodata_copy(VideoData* dst, unsigned char* src, int length);



