#include "stdafx.h"
#include "WebDefine.h"
#include <iostream>
#include "base.h"
#include <stdlib.h> 　　  
#include <stdio.h> 　
#include <sstream>
#include "CaptureAndMatting.h"


using std::string;
using std::wstring;

int roleID = 8;
double cameraPos[12][2];




float vectorDot(float x1, float y1, float x2, float y2)
{
	return (x1*x2 + y1*y2);
}

void GetCircle12PointsPos(float originX, float originY, float radius, double* cameraPosReturn, int cameraNum = 12)
{
	//第六个为x轴正轴
	//第三个为y轴正轴
	//以第六个摄像头的位置开始位置
	//cameraPos的行为对应的摄像头个数
	//  0 为 1号
	//  1 为 2号
	//  。。。为 。。。- 1 

	int angle = 30;
	int tempNum = 0;
	for (int i = 0; i < 12; i++)
	{
		tempNum = 6 - i;
		if (tempNum >= 0)
		{
			cameraPos[6 - i][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[6 - i][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		//else if (i == 6)
		//{
		//	cameraPos[11][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
		//	cameraPos[11][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		//}
		else if (i == 7)
		{
			cameraPos[11][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[11][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 8)
		{
			cameraPos[10][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[10][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 9)
		{
			cameraPos[9][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[9][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 10)
		{
			cameraPos[8][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[8][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 11)
		{
			cameraPos[7][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[7][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
	}
	int j;
	for (int i = 0, j = 0; i < 24; i += 2, j++)
	{
		cameraPosReturn[i] = cameraPos[j][0];
		cameraPosReturn[i + 1] = cameraPos[j][1];
	}
}

// 输入圆心，半径，以及圆周上摄像机的个数
float* GetCircle12PointsPos(float originX, float originY, float radius,int cameraNum=12 )
{
	//第六个为x轴正轴
	//第三个为y轴正轴
	//以第六个摄像头的位置开始位置
	//cameraPos的行为对应的摄像头个数
	//  0 为 1号
	//  1 为 2号
	//  。。。为 。。。- 1 

	float cameraPosReturn[24];
	int angle = 30;
	int tempNum = 0;
	for (int i = 0; i < 12; i++)
	{
		tempNum = 5 - i;
		if (tempNum>=0 )
		{
			cameraPos[5 - i][0] = originX + radius *cos(i*360/cameraNum *3.1415926/180);
			cameraPos[5 - i][1] = originX + radius *sin(i*360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 6)
		{
			cameraPos[11][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[11][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 7)
		{
			cameraPos[10][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[10][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 8)
		{
			cameraPos[9][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[9][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 9)
		{
			cameraPos[8][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[8][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 10)
		{
			cameraPos[7][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[7][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
		else if (i == 11)
		{
			cameraPos[6][0] = originX + radius *cos(i * 360 / cameraNum *3.1415926 / 180);
			cameraPos[6][1] = originX + radius *sin(i * 360 / cameraNum *3.1415926 / 180);
		}
	}
	int j;
	for (int i = 0,j=0; i < 24;i+=2,j++)
	{
		cameraPosReturn[i] = cameraPos[j][0];
		cameraPosReturn[i+1] = cameraPos[j][1];
	}
	return cameraPosReturn;
}

void lineAndCircleIntersectionReversePoints(float fx, float fz, float pointx, float pointz, float originx, float originz, float radius, double* reversePoint)
{
	float intersectionPoints[4];
	// 如果 跟坐标轴一致的话 直接退出 因为这时候已经知道该选择哪个摄像头了
	/*if ((fx > -0.0001 && fx<0.0001) ||  (fz > -0.0001 && fz<0.0001))
	{
	return nullptr;
	}*/

	float a;
	float b;
	float c;
	float c1;//过圆心的直线的截距
	// 取消在圆内的限制
	/*if (std::sqrt((pointx - originx)*(pointx - originx)
	+ (pointz - originz)*(pointz - originz)) > radius)
	{
	std::cout << "holens is not in the range of ricle" << std::endl;
	return nullptr;
	}*/


	c1 = (fx*originz + fz*originx);
	a = (fz*fz + fx*fx) / (fz*fz);
	b = -(2 * c1*fx + 2 * fz*originx*fx + 2 * originz*fz*fz) / (fz*fz);
	c = (c1*c1 + 2 * c1*originx*fz) / (fz*fz) + (originx*originx + originz*originz - radius*radius);

	//求交点   圆与直线的交点   过圆心的直线 
	intersectionPoints[1] = (-b + std::sqrt(b*b - 4 * a*c)) / (2 * a);// y1
	intersectionPoints[0] = (fx*intersectionPoints[1] - c1) / fz; //x1


	intersectionPoints[3] = (-b - std::sqrt(b*b - 4 * a*c)) / (2 * a);//y2
	intersectionPoints[2] = (fx*intersectionPoints[3] - c1) / fz; //x1

	float x1 = intersectionPoints[0] - originx;
	float z1 = intersectionPoints[1] - originz;

	/*/*/

	if (vectorDot(fx, fz, x1, z1) <0)
	{
		reversePoint[0] = intersectionPoints[0];
		reversePoint[1] = intersectionPoints[1];
	}
	float x2 = intersectionPoints[2] - originx;
	float z2 = intersectionPoints[3] - originz;

	if (vectorDot(fx, fz, x2, z2) < 0)
	{
		reversePoint[0] = intersectionPoints[2];
		reversePoint[1] = intersectionPoints[3];
	}
}

float* lineAndCircleIntersectionReversePoints(float fx, float fz, float pointx, float pointz, float originx, float originz, float radius)
{
	float reversePoint[2];
	float intersectionPoints[4];
	// 如果 跟坐标轴一致的话 直接退出 因为这时候已经知道该选择哪个摄像头了
	/*if ((fx > -0.0001 && fx<0.0001) ||  (fz > -0.0001 && fz<0.0001))
	{
		return nullptr;
	}*/

	float a;
	float b;
	float c;
	float c1;//过圆心的直线的截距
	// 取消在圆内的限制
	/*if (std::sqrt((pointx - originx)*(pointx - originx)
		+ (pointz - originz)*(pointz - originz)) > radius)
	{
		std::cout << "holens is not in the range of ricle" << std::endl;
		return nullptr;
	}*/


	c1 = (fx*originz + fz*originx);
	a = (fz*fz + fx*fx) / (fz*fz);
	b = -(2 * c1*fx + 2 * fz*originx*fx + 2 * originz*fz*fz) / (fz*fz);
	c = (c1*c1 + 2 * c1*originx*fz) / (fz*fz) + (originx*originx + originz*originz - radius*radius);

	//求交点   圆与直线的交点   过圆心的直线 
	intersectionPoints[1] = (-b + std::sqrt(b*b - 4 * a*c)) / (2 * a);// y1
	intersectionPoints[0] = (fx*intersectionPoints[1] - c1 ) / fz; //x1


	intersectionPoints[3] = (-b - std::sqrt(b*b - 4 * a*c)) / (2 * a);//y2
	intersectionPoints[2] = (fx*intersectionPoints[3] - c1) / fz; //x1

	float x1 = intersectionPoints[0] - originx  ;
	float z1 = intersectionPoints[1] - originz  ;

	/*/*/

	if (vectorDot(fx,fz,x1,z1) <0)
	{
		reversePoint[0] = intersectionPoints[0];
		reversePoint[1] = intersectionPoints[1];
	}
	float x2 = intersectionPoints[2] - originx  ;
	float z2 = intersectionPoints[3] - originz ;

	if (vectorDot(fx, fz, x2, z2) < 0)
	{
		reversePoint[0] = intersectionPoints[2];
		reversePoint[1] = intersectionPoints[3];
	}
	return reversePoint;
}



int getCameraIndex(double fx, double fz, double pointx, double pointz, double originx, double originz, double radius, int cameraNum = 12)
{
	int cameraIndex;
	float temp_fx = fx;
	float temp_fz = fz;
	fx = temp_fx / sqrt(temp_fx*temp_fx + temp_fz*temp_fz);
	fz = temp_fz / sqrt(temp_fx*temp_fx + temp_fz*temp_fz);
	if ((fx > -0.0001 && fx<0.0001) && (fz > -0.0001 && fz<0.0001))
	{
		std::cout << "法线设置不合理" << std::endl;
		return -1000;
	}
	else if (fx > -0.0001 && fx<0.0001)
	{
		if (fz>0)
		{
			cameraIndex = 9;
		}
		if (fz<0)
		{
			cameraIndex = 3;
		}
		return cameraIndex;
	}
	else if (fz > -0.0001 && fz<0.0001)
	{
		if (fx > 0)
		{
			cameraIndex = 0;
		}
		if (fx < 0)
		{
			cameraIndex = 6;
		}
		return cameraIndex;

	}
	else
	{
		double cameraPosOnCirlce[24];
		float cameraPosArray[12][2];
		float distances[12];
		float reversePointArray[2];
		double reversePoint[2];
	
		GetCircle12PointsPos(originx, originz, radius, cameraPosOnCirlce, cameraNum);

		
		for (int i = 0,j=0; i < 24;i+=2,j++)
		{
			cameraPosArray[j][0] = cameraPosOnCirlce[i];
			cameraPosArray[j][1] = cameraPosOnCirlce[i+1];
		}

		lineAndCircleIntersectionReversePoints(fx, fz, pointx, pointz, originx, originz, radius, reversePoint);
		for (int i = 0; i < 2;i++)
		{
			reversePointArray[i] = reversePoint[i];
		}

		for (int i = 0; i < 12;i++)
		{
			distances[i] = std::sqrt((reversePointArray[0] - cameraPosArray[i][0])*(reversePointArray[0] - cameraPosArray[i][0]) + (reversePointArray[1] - cameraPosArray[i][1])*(reversePointArray[1] - cameraPosArray[i][1]));
		}

		float b = distances[0];
		
		int d = 0;//  最小距离的下标

		for (int i = 0; i < 12;i++)
		{
			if (b > distances[i])
			{
				b = distances[i];
				
				d = i;
			}
		}
		cameraIndex = d;
		/*delete []cameraPosOnCirlce;
		delete []reversePoint;*/
		/*for (int i = 0; i < 12;i++)
		{
			cameraPosOnCirlce[i] = NULL;
		}
		for (int i = 0; i < 2;i++)
		{
			reversePoint[i] = NULL;
		}*/

		return cameraIndex;
	}
}


int str2num(string s)
{
	int num;
	std::stringstream ss(s);
	ss >> num;
	return num;
}
float srt2floatNum(string numberString)
{
	double num;
	std::stringstream ss(numberString);
	ss >> num;
	return num;
}

double* getFloatInString(string str, int index)
{
	string numberStringTest[3];
	double number[3];
	int count = 0;
	bool isNegtivenumber[3];
	for (int i = 0; i < 3;i++)
	{
		isNegtivenumber[i] = false;
	}
	for (int i = index; i < str.length(); i++)
	{
		if (str.at(i) == ',')
		{
			count++;
			continue;
		}
		
		/*if (str.at(i) == '-')
		{
			numberString[count].append(1, str.at(i));
		}*/
		if (str.at(i) >= '0' && str.at(i) <= '9'|| str.at(i) =='.' || str.at(i) == '-')
		{
			numberStringTest[count].append(1, str.at(i));
		}

		if (str.at(i) == '&')
		{
			break;
		}		
	}
	for (int i = 0; i < 3; i++)
	{
		number[i] = srt2floatNum(numberStringTest[i]);
	}
	return number;

}


string* getNumberString(string str, int index)
{
	string numberStringTest[3];
	char* numberChar[3];
	double number[3];
	int count = 0;
	bool isNegtivenumber[3];
	for (int i = 0; i < 3; i++)
	{
		isNegtivenumber[i] = false;
	}
	for (int i = index; i < str.length(); i++)
	{
		if (str.at(i) == ',')
		{
			count++;
			continue;
		}

		/*if (str.at(i) == '-')
		{
		numberString[count].append(1, str.at(i));
		}*/
		if (str.at(i) >= '0' && str.at(i) <= '9' || str.at(i) == '.' || str.at(i) == '-')
		{
			numberStringTest[count].append(1, str.at(i));
		}

		if (str.at(i) == '&')
		{
			break;
		}
	}
	return numberStringTest;
	
	/*for (int i = 0; i < 3; i++)
	{
		number[i] = srt2floatNum(numberStringTest[i]);
	}
	return number;*/

}

void getNumberFromString(string str, int index, double* number)
{
	string numberStringTest[3];
	int count = 0;
	bool isNegtivenumber[3];
	for (int i = 0; i < 3; i++)
	{
		isNegtivenumber[i] = false;
	}
	for (int i = index; i < str.length(); i++)
	{
		if (str.at(i) == ',')
		{
			count++;
			continue;
		}

		/*if (str.at(i) == '-')
		{
		numberString[count].append(1, str.at(i));
		}*/
		if (str.at(i) >= '0' && str.at(i) <= '9' || str.at(i) == '.' || str.at(i) == '-')
		{
			numberStringTest[count].append(1, str.at(i));
		}

		if (str.at(i) == '&')
		{
			break;
		}
	}


	//double number[3];
	for (int i = 0; i < 3; i++)
	{
		std::stringstream ss(numberStringTest[i]);
		ss >> number[i];
	}
	//return numberStringTest;

	/*for (int i = 0; i < 3; i++)
	{
	number[i] = srt2floatNum(numberStringTest[i]);
	}
	return number;*/

}

double* getNumberFromString(string str, int index)
{
	string numberStringTest[3];
	double number[3];
	int count = 0;
	bool isNegtivenumber[3];
	for (int i = 0; i < 3; i++)
	{
		isNegtivenumber[i] = false;
	}
	for (int i = index; i < str.length(); i++)
	{
		if (str.at(i) == ',')
		{
			count++;
			continue;
		}

		/*if (str.at(i) == '-')
		{
		numberString[count].append(1, str.at(i));
		}*/
		if (str.at(i) >= '0' && str.at(i) <= '9' || str.at(i) == '.' || str.at(i) == '-')
		{
			numberStringTest[count].append(1, str.at(i));
		}

		if (str.at(i) == '&')
		{
			break;
		}
	}


	//double number[3];
	for (int i = 0; i < 3; i++)
	{
		std::stringstream ss(numberStringTest[i]);
		ss >> number[i];
	}
	return number;

	//return numberStringTest;

	/*for (int i = 0; i < 3; i++)
	{
	number[i] = srt2floatNum(numberStringTest[i]);
	}
	return number;*/

}


int getOneIntInString(string str, int index)
{
	string numberString;
	int number;
	int count = 0;
	for (int i = index; i < str.length(); i++)
	{
		if (str.at(i) >= '0' && str.at(i) <= '9')
		{
			numberString.append(1, str.at(i));
		}

		if (str.at(i) == '&')
		{
			break;
		}
	}
	
	{
		number = str2num(numberString);
	}
	return number;
}




//int maintest(int argc, char * argv[]) 
//{
//	//////////////////////////////////////////////////////////////////////////
//	HRESULT hr = CoInitialize(NULL);
//	bool isfirst = true;
//
//	int closeDisplay = 0;
//	int whichCameraToMatting = 0;
//	int temp = 0;
//	int whichMattingAlgorithm = 0;
//	int tempMattinAlgorithm = 0;
//	int bgrORyuv = 0;
//	float scaleK = 0;
//	float tempScaleK = 0;
//
//	//////////////////////////////////////////////////////////////////////////
//
//
//    server testee_server;
//    short port = 8099;
//    size_t num_threads = 4;//使用4个线程来处理web请求
//
//  
//	/*printf("请输入你想要扣图的相机索引号,索引号为 0 - 11：\n");
//	scanf("%d", &whichCameraToMatting);*/
//	printf("请输入你想要数据缩放系数，缩放系数为 0 -2，0-1之间为缩小，1-2之间为放大：\n");
//	scanf("%f", &scaleK);
//	/*temp = whichCameraToMatting;*/
//	temp = roleID;
//	tempScaleK = scaleK;
//
//	printf("请输入你想输出的数据类型   0 bgr   1 yun：\n");
//	scanf("%d", &bgrORyuv);
//
//	printf("请输入想用的算法 0 bgr  1 hsv 2  bgr and picture  3 HSV_FULL：\n");
//	scanf("%d", &whichMattingAlgorithm);
//	tempMattinAlgorithm = whichMattingAlgorithm;
//
//
//    try {
//       
//
//		//////////////////////////////////////////////////////////////////////////
//		while (1)
//		{
//			if (isfirst)
//			{
//				SetRawImageDataScaleK(scaleK);
//				SetMattingAlgorithm(whichMattingAlgorithm);
//				//SetCameraIndex(whichCameraToMatting);
//				SetCameraIndex(roleID);
//				SetBGRorYUV(bgrORyuv);
//				init_capture_card_func();
//
//
//
//				// Total silence
//				testee_server.clear_access_channels(websocketpp::log::alevel::all);
//				testee_server.clear_error_channels(websocketpp::log::alevel::all);
//
//				// Initialize ASIO
//				testee_server.init_asio();
//				testee_server.set_reuse_addr(true);
//
//				// Register our message handler
//				testee_server.set_message_handler(bind(&on_message, &testee_server, ::_1, ::_2));
//
//				testee_server.set_socket_init_handler(bind(&on_socket_init, ::_1, ::_2));
//				testee_server.set_http_handler(bind(&on_http, &testee_server, ::_1));
//				// Listen on specified port with extended listen backlog
//				testee_server.set_listen_backlog(8192);
//				testee_server.listen(port);
//
//				// Start the server accept loop
//				testee_server.start_accept();
//
//				// Start the ASIO io_service run loop
//				if (num_threads == 1) {
//					testee_server.run();
//				}
//				else {
//					typedef websocketpp::lib::shared_ptr<websocketpp::lib::thread> thread_ptr;
//					std::vector<thread_ptr> ts;
//					for (size_t i = 0; i < num_threads; i++) {
//						ts.push_back(websocketpp::lib::make_shared<websocketpp::lib::thread>(&server::run, &testee_server));
//					}
//
//					for (size_t i = 0; i < num_threads; i++) {
//						ts[i]->join();
//					}
//				}
//				isfirst = false;
//
//			}
//			/*printf("请输入你想要扣图的相机索引号：\n");
//			scanf("%d", &whichCameraToMatting);
//			if (temp != whichCameraToMatting)
//			{
//				SetCameraIndex(whichCameraToMatting);
//			}
//			temp = whichCameraToMatting;*/
//
//
//			//printf("请输入你想要扣图的相机索引号：\n");
//			//scanf("%d", &whichCameraToMatting);
//			if (temp != roleID)
//			{
//				SetCameraIndex(roleID);
//			}
//			temp = roleID;
//
//		}
//		//////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//    } catch (websocketpp::exception const & e) {
//        std::cout << "exception: " << e.what() << std::endl;
//    }
//
//
//
//	CoUninitialize();
//}


