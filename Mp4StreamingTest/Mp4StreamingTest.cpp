// Mp4StreamingTest.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "WebDefine.h"
#include "GstMp4StreamingManager.h"
#include "base.h"
#include "accpetHttpData.h"
#include <boost/thread/thread.hpp>
#include <iostream>
#include<cv.h>
#include<queue>
#include"queue_safe.h"
#include"VideoData.h"

#include"CaptureAndMatting.h"
//#include "vld.h"

using namespace std;




gdface::mt::threadsafe_queue<VideoData> queuetestData;


void GstManagerOverNotify();

bool g_exit = false;
using std::string;
using std::wstring;

GstMp4StreamingManager* streamer = nullptr;
static HANDLE g_mainHandle = nullptr;


extern int roleID;
extern float cameraPos[12][2];
static const char *GetEventMessage(DWORD dwCtrlType)
{
	switch (dwCtrlType)
	{
	case CTRL_C_EVENT:
		return "CTRL_C_EVENT";
	case CTRL_BREAK_EVENT:
		return "CTRL_BREAK_EVENT";
	case CTRL_CLOSE_EVENT:
		return "CTRL_CLOSE_EVENT";
	case CTRL_LOGOFF_EVENT:
		return "CTRL_LOGOFF_EVENT";
	case CTRL_SHUTDOWN_EVENT:
		return "CTRL_SHUTDOWN_EVENT";
	}
	return "Unknown";
}

BOOL WINAPI HandlerRoutine(DWORD dwCtrlType)
{
	printf("%s, %s event received \n", __FUNCTION__, GetEventMessage(dwCtrlType));
	g_exit = true;

	return TRUE;
}

void GstManagerOverNotify()
{
	printf("%s, gst manager over notify receive \n", __FUNCTION__);
	SetEvent(g_mainHandle);
}

const string g_strRunPath = GetRunPathA();
// Define a callback to handle incoming messages
void on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg) {
	s->send(hdl, msg->get_payload(), msg->get_opcode());
}

void on_socket_init(websocketpp::connection_hdl, boost::asio::ip::tcp::socket & s) {
	boost::asio::ip::tcp::no_delay option(true);
	s.set_option(option);
}

void on_http(server* s, websocketpp::connection_hdl hdl)
{
	static int x = 0;
	std::cout << "come into http " << x++ << std::endl;
	try
	{
		server::connection_ptr con = s->get_con_from_hdl(hdl);
		con->get_uri()->get_query();
		con->get_uri()->get_resource();
		con->get_uri()->get_host_port();

		/*std::cout << "host_port****" << con->get_uri()->get_host_port() << std::endl;

		std::cout << "reource****" << con->get_uri()->get_resource() << std::endl;
		std::cout << "qurey *****" << con->get_uri()->get_query() << std::endl;*/

		websocketpp::http::parser::request rt = con->get_request();
		const string urltest = rt.get_uri();

		const string& strUri = rt.get_uri();
		const string& strMethod = rt.get_method();
		const string& strBody = rt.get_body();	//只针对post时有数据
		const string& strHost = rt.get_header("host");
		const string& strVersion = rt.get_version();

		std::cout << "接收到一个" << strMethod.c_str() << "请求：" << strUri.c_str() << "  线程ID=" << ::GetCurrentThreadId() << "  host = " << strHost << std::endl;
		std::cout << "方法为：" << strMethod << std::endl;


		if (strMethod.compare("POST") == 0)
		{//对于post提交的，直接返回跳转
			//websocketpp::http::parser::request r;
			con->set_status(websocketpp::http::status_code::value(websocketpp::http::status_code::found));
			con->append_header("location", "127.0.0.1:8099");
		}
		else if (strMethod.compare("GET") == 0)
		{
			if (strUri.compare("/") == 0)
			{//请求主页
				con->set_body("Hello Websocket!");
				con->set_status(websocketpp::http::status_code::value(websocketpp::http::status_code::ok));

			}

			string::size_type functionNamePosition;
			string functionName = "switchCamera?";//修改你要的函数名字
			functionNamePosition = strUri.find(functionName);
			string newStrUri;

			if (functionNamePosition != strUri.npos)
			{
				for (int i = 0; i < strUri.length(); i++)
				{
					if (strUri.at(i) != 32)
					{
						newStrUri.push_back(strUri.at(i));
					}
				}
				string::size_type passPosition;
				string::size_type passForwardPos;
				string::size_type rolePos;


				string postionName = "position=";//修改你要的函数名字
				string forwardName = "forward=";//修改你要的函数名字
				string roleName = "role=";

				passPosition = strUri.find(postionName);
				passForwardPos = strUri.find(forwardName);
				rolePos = strUri.find(roleName);
				//int* role;

				double forward[3];
				double testForward[3];


				double testPos[3];
				double testPosArray[3];


				if (passPosition != strUri.npos && passForwardPos != strUri.npos)
				{				
					getNumberFromString(newStrUri, int((int)passPosition + postionName.length()), testPos);
					testPosArray[0] = testPos[0];
					testPosArray[1] = testPos[1];
					testPosArray[2] = testPos[2];
					getNumberFromString(newStrUri, int((int)passForwardPos + forwardName.length()), forward);
					
					testForward[0] = forward[0];
					testForward[1] = forward[1];
					testForward[2] = forward[2];

					//  x轴  y轴  常规思路
					int cameraID = getCameraIndex(testForward[0], testForward[2], testPosArray[2], testPosArray[2], 0, 0, 1);
					std::cout << "实时处理的摄像头编号为：" << cameraID << std::endl;


					//cameraID = 3;

					streamer->SetVideoSrc(cameraID);
					
				}


				//std::cout << "postion" << "=" << testPosArray[0] << "  " << testPosArray[1] << "  " << testPosArray[2] << std::endl;
				std::cout << "forwad" << "=" << testForward[0] << "  " << testForward[1] << "  " << testForward[2] << std::endl;

				//std::cout << "roleID" << "==" << roleID << std::endl;



				con->set_body(newStrUri);

				con->set_status(websocketpp::http::status_code::value(websocketpp::http::status_code::ok));
			}

			else // 提示地址输入信息
			{
				static const string strImgPath = g_strRunPath + "server\\test.png";
				string strBuffer;
				int code = websocketpp::http::status_code::ok;
				if (ReadFileContent(strImgPath.c_str(), "rb", strBuffer))
				{
					con->set_body(strBuffer);
					con->append_header("Content-Type", "image/png");
				}
				else
				{//页面不存在，返回404
					code = websocketpp::http::status_code::not_found;
				}
				con->set_status(websocketpp::http::status_code::value(code));//HTTP返回码
			}
		}

		else
		{
			std::cout << "here else" << std::endl;

		}
	}
	catch (websocketpp::exception const & e)
	{
		std::cout << "exception: " << e.what() << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << "exception2: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "exception3: "  << std::endl;
	}
}

server testee_server;
short port = 8099;
size_t num_threads = 4;//使用4个线程来处理web请求

void holensConect(/*server testee_server, short port, size_t num_threads*/)
{
	
	testee_server.clear_access_channels(websocketpp::log::alevel::all);
	testee_server.clear_error_channels(websocketpp::log::alevel::all);
	// Initialize ASIO
	testee_server.init_asio();
	testee_server.set_reuse_addr(true);
	// Register our message handler
	testee_server.set_message_handler(bind(&on_message, &testee_server, ::_1, ::_2));
	testee_server.set_socket_init_handler(bind(&on_socket_init, ::_1, ::_2));
	testee_server.set_http_handler(bind(&on_http, &testee_server, ::_1));
	// Listen on specified port with extended listen backlog
	testee_server.set_listen_backlog(8192);
	testee_server.listen(port);
	// Start the server accept loop
	testee_server.start_accept();
	// Start the ASIO io_service run loop
	if (num_threads == 1) {
		testee_server.run();
	}
	else {
		typedef websocketpp::lib::shared_ptr<websocketpp::lib::thread> thread_ptr;
		std::vector<thread_ptr> ts;
		for (size_t i = 0; i < num_threads; i++) {
			ts.push_back(websocketpp::lib::make_shared<websocketpp::lib::thread>(&server::run, &testee_server));
		}

		for (size_t i = 0; i < num_threads; i++) {
			ts[i]->join();
		}
	}
}

 void matting()
{
	while(1){
		VideoData videodata123;
		//printf("2)===>ready pop data...\n");
		if (queuetestData.try_pop(videodata123))
		{
			//printf("3)===>pop data... ok\n");
			matting_data(videodata123);
			free(videodata123.data);
		

		}
	}	
}


int _tmain(int argc, _TCHAR* argv[])
{

	HRESULT hr = CoInitialize(NULL);



	SetConsoleCtrlHandler(HandlerRoutine, TRUE);
	g_mainHandle = CreateEvent(NULL, FALSE, FALSE, NULL);

	int whichCameraToMatting = 9;
	//int whichMattingAlgorithm = 2;
	int whichMattingAlgorithm = 0;

	int bgrORyuv = 1;
	int closeDisplay = 0;
	float scaleK = 0.10f;
	//int fragmentDuration = 1000;
	int fragmentDuration = 1000;

	int useMic = 1;
	int tempCameraID = 8;

	server testee_server;
	short port = 8099;
	size_t num_threads = 4;//使用4个线程来处理web请求

	//printf("请输入你想要扣图的相机索引号,索引号为 0 - 11：\n");
	//scanf("%d", &whichCameraToMatting);

	//printf("请输入你想要数据缩放系数，缩放系数为 0 -2，0-1之间为缩小，1-2之间为放大：\n");
	//scanf("%f", &scaleK);

	//printf("请输入你想输出的数据类型   0 bgr   1 yun：\n");
	//scanf("%d", &bgrORyuv);

	//printf("请输入想用的算法 0 bgr  1 hsv 2  bgr and picture  3 HSV_FULL：\n");
	//scanf("%d", &whichMattingAlgorithm);

	/*printf("请输入mp4切片时长，单位ms，如200/500/1000\n");
	scanf("%d", &fragmentDuration);
	if (fragmentDuration <= 0)
	{
		fragmentDuration = 1000;
	}*/

	/*printf("是否使用麦克风测试： 0为否, 1为是\n");
	scanf("%d", &useMic);*/

	
	boost::thread matt(&matting);
	//matt.timed_join(boost::posix_time::microseconds(1));
	//matt.join();
	//system("pause");
	for (int i = 0; i < 100; i++)
	{
		
		streamer = new (std::nothrow)GstMp4StreamingManager(GstManagerOverNotify);
		if (useMic == 1){
			streamer->SetAudioName("auto"); //"auto" "麦克风 (HD Webcam C310)" "麦克风 (HD Webcam C510)"
		}

		streamer->SetVideoSrc(whichCameraToMatting);

		streamer->SetMattingConfig(scaleK, bgrORyuv, whichMattingAlgorithm);

		streamer->SetFragmentDuration(fragmentDuration);

		//streamer->SetVideoConfig(20,1000*1000*1,60);
		streamer->Start();
		if (i==0)
		{
			boost::thread thrd(&holensConect);
			//thrd.timed_join(boost::posix_time::seconds(0));
			thrd.timed_join(boost::posix_time::microseconds(200));


			
			//thrd.timed_join(boost::posix_time::seconds(0));
			//thrd.timed_join(boost::posix_time::microseconds(1));

			//thrd.join();
		}
		


		WaitForSingleObject(g_mainHandle, INFINITE);;

		

		streamer->Stop();
		delete streamer;
	}

	

	CloseHandle(g_mainHandle);

	

	CoUninitialize();

	printf("%s, exit program \n", __FUNCTION__);
	return 0;
}

