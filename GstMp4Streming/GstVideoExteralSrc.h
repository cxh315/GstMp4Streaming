#ifndef GST_VIDEO_EXTERAL_SRC_H
#define GST_VIDEO_EXTERAL_SRC_H

#include <thread>
#include <gst/gst.h>



class GstVideoExteralSrcCB;
class GstVideoExteralSrc
{
public:
	GstVideoExteralSrc(GstVideoExteralSrcCB* cb);
	~GstVideoExteralSrc();

	//add capture and matting interfere here
	int SetVideoSrc(int index);// index 的范围为0 - 11 共12个摄像头

	//scaleK : 缩放系数为 0 -2，0-1之间为缩小，1-2之间为放大
	//videoType : 0 bgr   1 yuv
	//algorithm : 0 bgr  1 hsv 2  bgr and picture  3 HSV_FULL：
	int SetMattingConfig(float scaleK, int videoType, int algorithm);

	int GetVideoConfig(int& width, int& height, char*& colorspace);

	int Start();

	int Stop();

private:
	GstVideoExteralSrc(const GstVideoExteralSrc&);
	GstVideoExteralSrc& operator = (const GstVideoExteralSrc&);

	static void MattingDataCallBack(int width, int height, const unsigned char* data, int size, void* userData);

	void PushData(int width, int height, const unsigned char* data, int size);

	GMutex						m_mutex;
	GstVideoExteralSrcCB*		m_callback = nullptr;

	int							m_videoIndex = 0;
	int							m_videoType = 0;
	float						m_videoMattingScaleK = 1;
	int							m_videoMattingAlgorithm = 0;

	bool						m_isStarted = false;
};





#endif //GST_VIDEO_EXTERAL_SRC_H