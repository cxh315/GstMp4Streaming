#include <string.h>
#include <stdio.h>
#include <windows.h>
#include "gst/app/gstappsrc.h"
#include "GstFunctionCallBack.h"
#include "GstPipelineBuilder.h"

#define NEED_AUDIO 1

static const char *known_mimetypes[] = {
	"video/webm",
	"multipart/x-mixed-replace",
	NULL
};

GstPipelineBuilder::GstPipelineBuilder(GstPipelineBuilderCB* cb, char* path)
:m_callback(cb)
{
	int argc = 2;
	char* argv[] = { "gst_sdk", path }; //--gst-plugin-path=D:\\Mp4StreamSDK\\x64\\lib 
	char **args = argv;

	gst_init(&argc, &args);
	g_mutex_init(&m_mutex);
}

GstPipelineBuilder::~GstPipelineBuilder()
{
	if (m_contentType)
		g_free(m_contentType);

	if (m_audioName)
		g_free(m_audioName);

	if (m_videoFormat)
		g_free(m_videoFormat);

	g_mutex_clear(&m_mutex);
}

int GstPipelineBuilder::SetFragmentDuration(int duration)
{
	g_mutex_lock(&m_mutex);

	m_fragmentDuration = duration;

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstPipelineBuilder::SetVideoConfig(const gchar* format, int width, int height, int srcFrameRate, int dstFrameRate, int bitrate, int gop)
{
	g_mutex_lock(&m_mutex);

	if (format){
		m_videoFormat = g_strdup(format);
		m_width = width;
		m_height = height;
		m_srcFrameRate = srcFrameRate;
		m_dstFrameRate = dstFrameRate;
		m_bitrate = bitrate;
		m_gop;
	}

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstPipelineBuilder::SetAudioName(const char* name)
{
	g_mutex_lock(&m_mutex);

	if (name){
		m_audioName = g_strdup(name);
		m_isNeedAudio = true;
	}

	g_mutex_unlock(&m_mutex);

	return 0;
}

int GstPipelineBuilder::Init()
{
	g_mutex_lock(&m_mutex);

	if (!m_isInited)
	{
		if (CreatePipeline() < 0)
		{
			g_mutex_unlock(&m_mutex);
			return -1;
		}

		m_busWatchId = SetBusMessageCB(m_pipeline);

		g_print("%s, ready pipeline\n", __FUNCTION__);
		if (gst_element_set_state(m_pipeline, GST_STATE_READY) == GST_STATE_CHANGE_FAILURE) {
			g_print("%s, failed to set pipeline to ready\n", __FUNCTION__);
			g_mutex_unlock(&m_mutex);
			return -1;
		}

		m_isInited = true;
	}

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstPipelineBuilder::Release()
{
	g_mutex_lock(&m_mutex);

	if (m_busWatchId) {
		g_source_remove(m_busWatchId);
		m_busWatchId = 0;
	}

	if (m_pipeline){
		gst_object_unref(m_pipeline);
		m_pipeline = nullptr;
	}

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstPipelineBuilder::Test()
{
	m_pipeline = gst_pipeline_new("ceshi");

	m_pipelineData.videoSrc = gst_element_factory_make("appsrc", "app_video_src");
	m_pipelineData.videoConvert = gst_element_factory_make("videoconvert", "video_convert");
	m_pipelineData.clockOverlay = gst_element_factory_make("clockoverlay", "video_clock_lay");
	m_pipelineData.videoEnc = gst_element_factory_make("openh264enc", "video_enc");
	m_pipelineData.videoParse = gst_element_factory_make("h264parse", "h264_parse");
	m_pipelineData.muxer = gst_element_factory_make("flvmux", "muxer");
	m_pipelineData.httpsink = gst_element_factory_make("filesink", "video_sink");

	const gchar* videoSrcName = gst_element_get_name(m_pipelineData.videoSrc);
	if (strncmp(videoSrcName, "app_video_src", strlen("app_video_src")) == 0){

		GstCaps* caps = gst_caps_new_simple("video/x-raw",
			"format", G_TYPE_STRING, "I420",
			"width", G_TYPE_INT, 640,
			"height", G_TYPE_INT, 480,
			nullptr);


		g_object_set(m_pipelineData.videoSrc,
			"caps", caps,
			"do-timestamp", true,
			//"is-live", true,
			nullptr);

		if (caps)
			gst_caps_unref(caps);
	}

	g_object_set(m_pipelineData.clockOverlay,
		"shaded-background", true,
		"font-desc", "Sans 38",
		nullptr);

	//create http streaming function element
	g_object_set(m_pipelineData.muxer,
		"streamable", true,
		"fragment-duration", m_fragmentDuration,
		nullptr);

	g_object_set(m_pipelineData.httpsink, "location", "E:\\1.flv", nullptr);

	// add elements
	gst_bin_add_many(GST_BIN(m_pipeline), m_pipelineData.videoSrc, m_pipelineData.videoConvert, m_pipelineData.clockOverlay, 
		m_pipelineData.videoEnc, m_pipelineData.videoParse, m_pipelineData.muxer, m_pipelineData.httpsink, nullptr);
	// link elements
	if (!gst_element_link_many(m_pipelineData.videoSrc, m_pipelineData.videoConvert, m_pipelineData.videoEnc, m_pipelineData.videoParse, nullptr)){
		g_print("%s, link video elements failed \n", __FUNCTION__);
		return -1;
	}

	if (!gst_element_link_many(m_pipelineData.muxer, m_pipelineData.httpsink, nullptr)){
		g_print("%s, link video elements failed \n", __FUNCTION__);
		return -1;
	}

	// link video parse to muxer
	GstPad* videoParseSrcPad = gst_element_get_static_pad(m_pipelineData.videoParse, "src");
	GstPad* muxerVideoSinkPad = gst_element_get_request_pad(m_pipelineData.muxer, "video");
	if (!videoParseSrcPad || !muxerVideoSinkPad){
		g_print("%s, get video function pad failed \n", __FUNCTION__);
		return -1;
	}

	if (gst_pad_link(videoParseSrcPad, muxerVideoSinkPad) != GST_PAD_LINK_OK) {
		g_print("%s, link video function pad to muxer failed \n", __FUNCTION__);
		return -1;
	}

	if (gst_element_set_state(m_pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
		g_print("%s, failed to start pipeline\n", __FUNCTION__);
		return -1;
	}

	m_isRunning = true;

	return 0;
}

int GstPipelineBuilder::SetPlay()
{
	g_mutex_lock(&m_mutex);

	if (!m_isInited)
	{
		g_mutex_unlock(&m_mutex);
		return -1;
	}

	if (!m_isRunning)
	{
		g_print("%s, start pipeline\n", __FUNCTION__);
		if (gst_element_set_state(m_pipeline, GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
			g_print("%s, failed to start pipeline\n", __FUNCTION__);
			g_mutex_unlock(&m_mutex);
			if (m_callback)
			{
				m_callback->PipelineOver();
			}

			return -1;
		}

		m_isRunning = true;
	}

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstPipelineBuilder::SetStop()
{
	g_mutex_lock(&m_mutex);

	if (!m_isInited)
	{
		g_mutex_unlock(&m_mutex);
		return -1;
	}

	g_print("%s, stop pipeline \n", __FUNCTION__);
	if (m_state > GST_STATE_NULL)	{
		gst_element_set_state(m_pipeline, GST_STATE_NULL);
	}

	m_isRunning = false;

	g_mutex_unlock(&m_mutex);
	return 0;
}

void GstPipelineBuilder::PushVideoData(int width, int height, const guchar* data, int size)
{
	g_mutex_lock(&m_mutex);

	if (m_isRunning){
		const gchar* videoSrcName = gst_element_get_name(m_pipelineData.videoSrc);
		if (strncmp(videoSrcName, "app_video_src", strlen("app_video_src")) == 0){
			
			GstBuffer *buf = nullptr;
			GstMapInfo map;


			if (!m_saveFirstFrame)
			{
				m_saveFirstFrame = true;
			}

			buf = gst_buffer_new_and_alloc(size);
			gst_buffer_map(buf, &map, GST_MAP_WRITE);
			memcpy(map.data, data, size);
			gst_buffer_unmap(buf, &map);

 			_SYSTEMTIME time;
 			GetLocalTime(&time);
 			//g_print("%s, [%02u:%02u:%02u:%03u] push data to app video src \n", __FUNCTION__, time.wHour, time.wSecond, time.wSecond, time.wMilliseconds);

			int ret = gst_app_src_push_buffer((GstAppSrc*)m_pipelineData.videoSrc, buf);
			if (ret != GST_FLOW_OK){
				g_print("%s, app video src push buffer failed with %d", __FUNCTION__, ret);
			}
		}
	}

	g_mutex_unlock(&m_mutex);
}

gpointer GstPipelineBuilder::GetSocketSink()
{
	gpointer socketSink = nullptr;

	g_mutex_lock(&m_mutex);
	socketSink = m_pipelineData.httpsink;
	g_mutex_unlock(&m_mutex);

	return socketSink;
}

const gchar* GstPipelineBuilder::GetContentType()
{
	gchar* contentType = nullptr;

	g_mutex_lock(&m_mutex);
	contentType = m_contentType;
	g_mutex_unlock(&m_mutex);

	return contentType;
}

gboolean GstPipelineBuilder::OnMyBusCallback(GstBus *bus, GstMessage *message, gpointer data)
{
	GstPipelineBuilder* builder = (GstPipelineBuilder*)data;
	if (builder)
	{
		return builder->MyBusCallback(bus, message);
	}

	return TRUE;
}

gboolean GstPipelineBuilder::MyBusCallback(GstBus *bus, GstMessage *message)
{
	switch (GST_MESSAGE_TYPE(message)) {
	case GST_MESSAGE_ERROR:{
		gchar *debug;
		GError *err;

		gst_message_parse_error(message, &err, &debug);
		g_print("%s, error %s\n", __FUNCTION__, err->message);
		g_error_free(err);
		g_free(debug);
		if (m_callback){
			m_callback->PipelineOver();
		}

		break;
	}
	case GST_MESSAGE_WARNING:{
		gchar *debug;
		GError *err;

		gst_message_parse_warning(message, &err, &debug);
		g_print("%s, warning %s\n", __FUNCTION__, err->message);
		g_error_free(err);
		g_free(debug);
		break;
	}
	case GST_MESSAGE_STATE_CHANGED:
		StateChanged(message);
		break;
	case GST_MESSAGE_EOS:{
		g_print("%s, eos\n", __FUNCTION__);
		if (m_callback){
			m_callback->PipelineOver();
		}
	}
	default:
		break;
	}

	return TRUE;
}

void GstPipelineBuilder::OnStreamCapsChanged(GObject *obj, GParamSpec *pspec, gpointer data)
{
	GstPipelineBuilder* builder = (GstPipelineBuilder*)data;
	if (builder)
	{
		builder->StreamCapsChanged(obj, pspec);
	}
}

void GstPipelineBuilder::StreamCapsChanged(GObject *obj, GParamSpec *pspec)
{
	GstPad *src_pad = nullptr;
	GstCaps *src_caps = nullptr;
	GstStructure *gstrc = nullptr;

	src_pad = (GstPad *)obj;
	if (!src_pad){
		return;
	}
	
	src_caps = gst_pad_get_current_caps(src_pad);
	if (!src_caps){
		return;
	}
	gstrc = gst_caps_get_structure(src_caps, 0);
	if (!gstrc){
		return;
	}
	/*
	* Include a Content-type header in the case we know the mime
	* type is OK in HTTP. Required for MJPEG streams.
	*/
	int i = 0;
	const gchar *mimetype = gst_structure_get_name(gstrc);
	while (mimetype && known_mimetypes[i] != NULL)
	{
		if (strcmp(mimetype, known_mimetypes[i]) == 0)
		{
			if (m_contentType)
				g_free(m_contentType);

			/* Handle the (maybe not so) especial case of multipart to add boundary */
			if (strcmp(mimetype, "multipart/x-mixed-replace") == 0 &&
				gst_structure_has_field_typed(gstrc, "boundary", G_TYPE_STRING))
			{
				const gchar *boundary = gst_structure_get_string(gstrc, "boundary");
				m_contentType = g_strdup_printf("Content-Type: "
					"multipart/x-mixed-replace;boundary=--%s\r\n", boundary);
			}
			else
			{
				m_contentType = g_strdup_printf("Content-Type: %s\r\n", mimetype);
			}
			g_print("%s, %s", __FUNCTION__, m_contentType);
			break;
		}
		i++;
	}

	if (src_caps){
		gst_caps_unref(src_caps);
	}

	if (m_callback)
	{
		m_callback->CapResolv();
	}
}

void GstPipelineBuilder::OnClientSocketRemoved(GstElement * element, GSocket * socket, gpointer data)
{
	GstPipelineBuilder* builder = (GstPipelineBuilder*)data;
	if (builder)
	{
		builder->ClientSocketRemoved(element, socket);
	}
}

void GstPipelineBuilder::ClientSocketRemoved(GstElement * element, GSocket * socket)
{
	if (m_callback)
	{
		m_callback->SocketRemove(socket);
	}
}

void GstPipelineBuilder::StateChanged(GstMessage *msg)
{
	GstState old_state, new_state, pending_state;
	gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state);
	if (GST_MESSAGE_SRC(msg) == GST_OBJECT(m_pipeline)) {
		g_print("%s, pipeline state change from %s to %s \n", __FUNCTION__, gst_element_state_get_name(m_state), gst_element_state_get_name(new_state));
		m_state = new_state;
	}
}

int GstPipelineBuilder::CreatePipeline()
{
	if (CreateElement(m_pipeline, &m_pipelineData, m_isNeedAudio) < 0){
		g_print("%s, create element failed \n", __FUNCTION__);
		return -1;
	}

	if (ConfingElement(&m_pipelineData) < 0){
		g_print("%s, config element failed", __FUNCTION__);
		return -1;
	}

	if (LinkElement(m_pipeline, &m_pipelineData, m_isNeedAudio) < 0)
	{
		g_print("%s, link element failed", __FUNCTION__);
		return -1;
	}

	return 0;
}

int GstPipelineBuilder::CreateElement(GstElement*& pipeline, ElementData* data, bool needAudio)
{
	pipeline = gst_pipeline_new("mp4_streaming");
	if (!pipeline){
		g_print("%s, create pipeline failed \n", __FUNCTION__);
		return -1;
	}

	//create video function element
	if (m_isAppSrc){
		data->videoSrc = gst_element_factory_make("appsrc", "app_video_src");
	}
	else{
		data->videoSrc = gst_element_factory_make("videotestsrc", "test_video_src");
	}
	data->videoConvert = gst_element_factory_make("videoconvert", "video_convert");
	data->videoRate = gst_element_factory_make("videorate", "video-rate-caps");
	data->videoRateCaps = gst_element_factory_make("capsfilter", "capsfilter-videorate");
	data->clockOverlay = gst_element_factory_make("clockoverlay", "video_clock_layer");
	data->videoEnc = gst_element_factory_make("openh264enc", "video_enc");
	data->videoParse = gst_element_factory_make("h264parse", "video_parse");
	if (!data->videoSrc || !data->videoConvert || !data->videoRate || !data->videoRateCaps || !data->clockOverlay || !data->videoEnc || !data->videoParse){
		g_print("%s, create video function element failed \n", __FUNCTION__);
		return -1;
	}

	//create audio function element
	if (needAudio){
		if (strncmp(m_audioName, "auto", strlen("auto")) != 0){
			data->audioSrc = gst_element_factory_make("directsoundsrc", "audio_src");
		}else{
			data->audioSrc = gst_element_factory_make("autoaudiosrc", "audio_src"); //
		}

		data->audioConvert = gst_element_factory_make("audioconvert", "audio_convert");
		data->audioResample = gst_element_factory_make("audioresample", "audio_resample");
		data->audioEnc = gst_element_factory_make("voaacenc", "audio_enc");
		data->audioParse = gst_element_factory_make("aacparse", "audio_parse");
		if (!data->audioSrc || !data->audioConvert || !data->audioResample || !data->audioEnc || !data->audioParse){
			g_print("%s, create audio function element failed \n", __FUNCTION__);
			return -1;
		}
	}

	//create http streaming function element
	data->muxer = gst_element_factory_make("qtmux", "muxer");
	data->httpsink = gst_element_factory_make("multisocketsink", "http_sink"); // multisocketsink filesink
	if (!data->muxer || !data->httpsink){
		g_print("%s, create http streaming function failed \n", __FUNCTION__);
		return -1;
	}

	return 0;
}

int GstPipelineBuilder::ConfingElement(const ElementData* data)
{
	//create video function element
	const gchar* videoSrcName =  gst_element_get_name(data->videoSrc);
	if (strncmp(videoSrcName, "app_video_src", strlen("app_video_src")) == 0){
		GstCaps* caps = gst_caps_new_simple("video/x-raw",
			"format", G_TYPE_STRING, m_videoFormat,
			"width", G_TYPE_INT, m_width,
			"height", G_TYPE_INT, m_height,
			"framerate", GST_TYPE_FRACTION, m_srcFrameRate, 1,
			nullptr);


		//if appsrc is not live, the async of multisocketsink is false
		g_object_set(data->videoSrc,
			"caps", caps,
			"do-timestamp", true,
			nullptr);

		if (caps)
			gst_caps_unref(caps);
	}

	if (data->videoRateCaps){
		GstCaps *caps = NULL;
		caps = gst_caps_new_simple("video/x-raw", "framerate", GST_TYPE_FRACTION, m_dstFrameRate, 1, NULL);
		if (caps){
			g_object_set(data->videoRateCaps, "caps", caps, NULL);
			gst_caps_unref(caps);
		}
	}

	g_object_set(data->clockOverlay,
		"shaded-background", true,
		"font-desc", "Sans 38",
		nullptr);

	g_object_set(data->videoEnc, 
		"bitrate", m_bitrate, //��������
		"gop-size", m_gop, //����gop��
		nullptr);

	//create audio function element
	if (m_isNeedAudio){
		if (strncmp(m_audioName, "auto", strlen("auto")) != 0){
			g_object_set(data->audioSrc,
				"device-name", m_audioName,
				"provide-clock", true,
				"do-timestamp", true,
				nullptr);
		}
	}

	//create http streaming function element
	g_object_set(data->muxer, 
		"streamable", true, 
		"fragment-duration", m_fragmentDuration,
		nullptr);

	g_object_set(data->httpsink, 
		"unit-format", GST_FORMAT_TIME, 
		"units-max", (gint64)7 * GST_SECOND, 					
		"units-soft-max", (gint64)3 * GST_SECOND,
		"recover-policy", 3 /* keyframe */,
		"timeout", (guint64)10 * GST_SECOND,
		"sync-method", 1 /* next-keyframe */,
		"async",  false,
		nullptr);

	return 0;
}

int GstPipelineBuilder::LinkElement(const GstElement* pipeline, const ElementData* data, bool needAudio)
{
	int ret = -1;
	GstBus *bus = nullptr;
	GstPad *videoParseSrcPad = nullptr;
	GstPad *audioParseSrcPad = nullptr;
	GstPad	*muxerVideoSinkPad = nullptr;
	GstPad *muxerAudioSinkPad = nullptr;
	GstPad* muxerSrcPad = nullptr;

	// add elements
	gst_bin_add_many(GST_BIN(pipeline), data->videoSrc, data->videoRate, data->videoRateCaps, data->videoConvert, data->clockOverlay, data->videoEnc, data->videoParse, nullptr);

	if (needAudio){	
		gst_bin_add_many(GST_BIN(pipeline), data->audioSrc, data->audioConvert, data->audioResample, data->audioEnc, data->audioParse, nullptr);
	}

	gst_bin_add_many(GST_BIN(pipeline), data->muxer, data->httpsink, nullptr);

	// link elements
	if (!gst_element_link_many(data->videoSrc, data->videoConvert, data->clockOverlay, data->videoEnc, data->videoParse, nullptr)){
		g_print("%s, link video elements failed \n", __FUNCTION__);
		goto fail;
	}

	if (needAudio && !gst_element_link_many(data->audioSrc, data->audioConvert, data->audioResample, data->audioEnc, data->audioParse, nullptr)){
		g_print("%s, link audio elements failed \n", __FUNCTION__);
		goto fail;
	}

	if (!gst_element_link_many(data->muxer, data->httpsink, nullptr)){
		g_print("%s, link http streaming elements failed \n", __FUNCTION__);
		goto fail;
	}

	// link video parse to muxer
	videoParseSrcPad = gst_element_get_static_pad(data->videoParse, "src");
	muxerVideoSinkPad = gst_element_get_request_pad(data->muxer, "video_%u");
	if (!videoParseSrcPad || !muxerVideoSinkPad){
		g_print("%s, get video function pad failed \n", __FUNCTION__);
		goto fail;
	}

	if (gst_pad_link(videoParseSrcPad, muxerVideoSinkPad) != GST_PAD_LINK_OK ) {
		g_print("%s, link video function pad to muxer failed \n", __FUNCTION__);
		goto fail;
	}
	
	// link audio parse to muxer
	if (needAudio){
		audioParseSrcPad = gst_element_get_static_pad(data->audioParse, "src");
		muxerAudioSinkPad = gst_element_get_request_pad(data->muxer, "audio_%u");
		if (!audioParseSrcPad || !muxerAudioSinkPad){
			g_print("%s, get audio function pad failed \n", __FUNCTION__);
			goto fail;
		}

		if (gst_pad_link(audioParseSrcPad, muxerAudioSinkPad) != GST_PAD_LINK_OK){
			g_print("%s, link audio function pad to muxer failed \n", __FUNCTION__);
			goto fail;
		}
	}

	//onStreamCapsChanged
	muxerSrcPad = gst_element_get_static_pad(data->muxer, "src");
	if (!muxerSrcPad)
	{
		g_print("%s, get muxer static src pad failed \n", __FUNCTION__);
		goto fail;
	}

	m_contentType = g_strdup("");
	g_signal_connect(muxerSrcPad, "notify::caps", G_CALLBACK(OnStreamCapsChanged), this);

	//onClientSocketRemoved
	g_signal_connect(data->httpsink, "client-socket-removed",G_CALLBACK(OnClientSocketRemoved), this);

	ret = 0;

fail:
	if (videoParseSrcPad)
		gst_object_unref(videoParseSrcPad);
	if (audioParseSrcPad)
		gst_object_unref(audioParseSrcPad);
	if (muxerVideoSinkPad)
		gst_object_unref(muxerVideoSinkPad);
	if (muxerAudioSinkPad)
		gst_object_unref(muxerAudioSinkPad);
	if (muxerSrcPad)
		gst_object_unref(muxerSrcPad);

	return ret;
}

int GstPipelineBuilder::SetBusMessageCB(GstElement* pipeline)
{
	int busWatchId = 0;
	GstBus *bus = nullptr;

	bus = gst_pipeline_get_bus(GST_PIPELINE(m_pipeline));
	busWatchId = gst_bus_add_watch(bus, OnMyBusCallback, this);
	gst_object_unref(bus);

	return busWatchId;
}