#ifndef GST_HTTP_SERVER_H
#define GST_HTTP_SERVER_H

#include <gio/gio.h>
#include <thread>

typedef struct _Client
{
public:
	gchar *name;
	GSocketConnection *connection;
	GSocket *socket;
	GInputStream *istream;
	GOutputStream *ostream;
	GSource *isource;
	GSource *tosource;
	GByteArray *current_message;
	gchar *http_version;
	gboolean waiting_200_ok;
	void* userData;

	_Client()
	{
		name = nullptr;
		connection = nullptr;
		socket = nullptr;
		istream = nullptr;
		ostream = nullptr;
		isource = nullptr;
		tosource = nullptr;
		current_message = nullptr;
		http_version = nullptr;
		waiting_200_ok = false;
		userData = nullptr;
	}
}Client;

class GstHttpServerCB;
class GstHttpServer
{
public:
	GstHttpServer(GstHttpServerCB* cb);
	~GstHttpServer();

	int Start(const gchar* ip, int port);

	int Stop();

	void CapResolve();

	void SocketRemove(void* socket);

private:
	GstHttpServer(const GstHttpServer&);
	GstHttpServer& operator = (const GstHttpServer&);

	static unsigned int _stdcall ServerThread(void* param);

	void ServerStart();

	static gboolean OnNewConnection(GSocketService * service, GSocketConnection * connection, GObject * source, gpointer data);

	gboolean NewConnection(GSocketService * service, GSocketConnection * connection, GObject * source);

	static gboolean OnReadBytes(GPollableInputStream * stream, Client * client);

	gboolean ReadBytes(GPollableInputStream * stream, Client * client);

	static gboolean OnTimeOut(Client * client);

	gboolean TimeOut(Client * client);

	void WriteBytes(Client * client, const gchar * data, guint len);

	void ClientMessage(Client * client, const gchar * data, guint len);

	void RemoveClient(Client * client);

	void SendResponse200Ok(Client * client);
	
	void SendResponse404NotFound(Client * client);

	GMutex					m_mutex;
	GMutex					m_clientMutex;
	GMutex					m_capMutex;

	std::thread				m_thread;
	GMainLoop*			m_loop = nullptr;

	GstHttpServerCB*	m_callback = nullptr;
	GSocketService*	m_service = nullptr;
	gchar*					m_ip = nullptr;
	int							m_port = 80;

	GList*	m_clients = nullptr;
	bool		m_capResolved = false;
};


#endif //GST_HTTP_SERVER_H