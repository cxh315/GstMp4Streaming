#ifndef  GST_FUNCTION_CALL_BACK
#define GST_FUNCTION_CALL_BACK

class GstHttpServerCB
{
public:
	virtual void* GetSocketSink() = 0;

	virtual const char* GetContentType() = 0;

	virtual void ConnectOk() = 0;

protected:
	virtual ~GstHttpServerCB(){}
};


class GstPipelineBuilderCB
{
public:
	virtual void CapResolv() = 0;

	virtual void SocketRemove(void* socket) = 0;

	virtual void PipelineOver() = 0;

protected:
	virtual ~GstPipelineBuilderCB(){}
};

class GstVideoExteralSrcCB
{
public:
	virtual void PushVideo(int width, int height, const unsigned char* data, int size) = 0;

protected:
	virtual ~GstVideoExteralSrcCB(){}
};

#endif //GST_FUNCTION_CALL_BACK