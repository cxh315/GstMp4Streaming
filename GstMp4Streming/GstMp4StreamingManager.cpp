#include <iostream>
#include <windows.h>
#include "GstPipelineBuilder.h"
#include "GstHttpServer.h"
#include "GstVideoExteralSrc.h"
#include "GstMp4StreamingManager.h"

static size_t StringBackFind(std::string src, std::string subs[], int subsCount, int& subIndex, int times)
{
	std::string tmpSrc = src;

	if (subsCount <= 0 || times <= 0)
	{
		return 0;
	}

	for (int i = 0; i < subsCount; i++)
	{
		const char* sub = subs[i].c_str();

		for (int j = 0; j < times; j++)
		{
			size_t pos = tmpSrc.rfind(sub);
			if (pos != tmpSrc.npos)
			{
				tmpSrc.erase(tmpSrc.begin() + pos, tmpSrc.end());
				if (j == times - 1)
				{
					subIndex = i;
					return pos;
				}
			}
			else
			{
				break;
			}
		}
	}

	return 0;
}

GstMp4StreamingManager::GstMp4StreamingManager(ManagerOverCB cb)
:m_managerOverCB(cb)
{
	Init();
}

GstMp4StreamingManager::~GstMp4StreamingManager()
{
	if (m_pipelineBuilder){
		delete m_pipelineBuilder;
		m_pipelineBuilder = nullptr;
	}

	if (m_httpServer){
		delete m_httpServer;
		m_httpServer = nullptr;
	}

	if (m_videoExteralSrc){
		delete m_videoExteralSrc;
		m_videoExteralSrc = nullptr;
	}

	if (m_ip){
		g_free(m_ip);
		m_ip = nullptr;
	}

	if (m_audioName){
		g_free(m_audioName);
		m_audioName = nullptr;
	}
}

int GstMp4StreamingManager::SetVideoSrc(int index)
{
	if (!m_isInited){
		g_print("%s, you need init it \n", __FUNCTION__);
	}

	if (m_videoExteralSrc){
		m_videoExteralSrc->SetVideoSrc(index);
	}

	m_videoIndex = index;
	return 0;
}

int GstMp4StreamingManager::SetMattingConfig(float scaleK, int videoType, int algorithm)
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	m_videoMattingScaleK = scaleK;
	m_videoSrcType = videoType;
	m_videoMattingAlgorithm = algorithm;

	return 0;
}

int GstMp4StreamingManager::SetVideoConfig(int frameRate, int bitRate, int gop)
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	m_videoDstFrameRate = frameRate;
	m_videoBitRate = bitRate;
	m_videoGop = gop;

	return 0;
}

int GstMp4StreamingManager::SetAudioName(const char* name)
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	if (name){
		m_audioName = g_strdup(name);
	}

	return 0;
}

int GstMp4StreamingManager::SetFragmentDuration(int duration)
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	m_duration = duration;
	return 0;
}

int GstMp4StreamingManager::SetHttp(const char* ip, int port)
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	if (ip){
		m_ip = g_strdup(ip);
	}

	m_port = port;

	return 0;
}

int GstMp4StreamingManager::Start()
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	int width = 0;
	int height = 0;
	char* colorSpace = nullptr;

	m_videoExteralSrc->SetVideoSrc(m_videoIndex);
	m_videoExteralSrc->SetMattingConfig(m_videoMattingScaleK, m_videoSrcType, m_videoMattingAlgorithm);
	m_videoExteralSrc->Start();

	int tryGetVideoResolutionTime = 1500;
	while (1){
		if (m_videoExteralSrc->GetVideoConfig(width, height, colorSpace) == 0){
			printf("%s, try get video config times", __FUNCTION__);
			break;
		}

		Sleep(10);

		tryGetVideoResolutionTime--;
		if(tryGetVideoResolutionTime <= 0){
			g_print("%s, get video resolution failed", __FUNCTION__);
			return -1;
		}
	}

#if 0
	m_pipelineBuilder->Test();
#else
	if (m_audioName){
		m_pipelineBuilder->SetAudioName(m_audioName);
	}

	m_pipelineBuilder->SetFragmentDuration(m_duration);
	m_pipelineBuilder->SetVideoConfig(colorSpace, width, height, m_videoSrcFrameRate,m_videoDstFrameRate, m_videoBitRate, m_videoGop);
	if (m_pipelineBuilder->Init() < 0){
		g_print("%s, pipeline builder init failed", __FUNCTION__);
		return -1;
	}

	if (m_httpServer->Start(m_ip, m_port) < 0){
		g_print("%s, http server start failed \n", __FUNCTION__);
		return -1;
	}
#endif

	return 0;
}

int GstMp4StreamingManager::Stop()
{
	if (!m_isInited){
		g_print("%s, you need init it", __FUNCTION__);
	}

	m_httpServer->Stop();
	m_pipelineBuilder->SetStop();
	m_pipelineBuilder->Release();
	m_videoExteralSrc->Stop();

	return 0;
}

//GstPipelineBuilderCB
void GstMp4StreamingManager::CapResolv()
{
	if (m_httpServer){
		m_httpServer->CapResolve();
	}
}

void GstMp4StreamingManager::SocketRemove(void* socket)
{
	if (m_httpServer){
		m_httpServer->SocketRemove(socket);
	}

	if (m_managerOverCB)
		m_managerOverCB();
}

void GstMp4StreamingManager::PipelineOver()
{

}

//GstHttpServerCB
void* GstMp4StreamingManager::GetSocketSink()
{
	if (m_pipelineBuilder){
		return m_pipelineBuilder->GetSocketSink();
	}

	return nullptr;
}

const char* GstMp4StreamingManager::GetContentType()
{
	if (m_pipelineBuilder){
		return m_pipelineBuilder->GetContentType();
	}

	return nullptr;
}

void GstMp4StreamingManager::ConnectOk()
{
	if (m_pipelineBuilder){
		m_pipelineBuilder->SetPlay();
	}
}

void GstMp4StreamingManager::PushVideo(int width, int height, const unsigned char* data, int size)
{
	if (m_pipelineBuilder){
		m_pipelineBuilder->PushVideoData(width, height, data, size);
	}
}

const char* GstMp4StreamingManager::GetSdkPath()
{
	std::string subs[] = { "/", "\\" };
	int subsCount = sizeof(subs) / sizeof(subs[0]);
	int separatorIndex = -1;
	char basePath[MAX_PATH] = { 0 };
	char sdkPath[MAX_PATH] = { 0 };
	std::string strBasePath;
	::GetModuleFileNameA(NULL, basePath, MAX_PATH);
	strBasePath = basePath;

	size_t basePos = StringBackFind(strBasePath, subs, subsCount, separatorIndex, 1);
	if (basePos <= 0 || separatorIndex == -1){
		printf("%s, StringBackFind failed, program path is %s \n", __FUNCTION__, strBasePath.c_str());
		return nullptr;
	}

	strBasePath.erase(strBasePath.begin() + basePos, strBasePath.end());
	sprintf_s(sdkPath, MAX_PATH, "--gst-plugin-path=%s\\lib", strBasePath.c_str());

	return sdkPath;
}

int GstMp4StreamingManager::Init()
{
	const char* sdkDir = GetSdkPath();
	if (!sdkDir){
		g_print("%s, get gstreamer sdk path failed \n", __FUNCTION__);
		return -1;
	}

	char tmpPath[MAX_PATH];
	strcpy_s(tmpPath, MAX_PATH, sdkDir);

	m_pipelineBuilder = new (std::nothrow) GstPipelineBuilder(this, tmpPath);
	if (!m_pipelineBuilder){
		g_print("%s, new GstPipelineBuilder failed", __FUNCTION__);
		return -1;
	}

	m_httpServer = new (std::nothrow) GstHttpServer(this); 
	if (!m_httpServer){
		g_print("%s, new GstHttpServer failed", __FUNCTION__);
		return -1;
	}

	m_videoExteralSrc = new (std::nothrow)GstVideoExteralSrc(this);
	if (!m_videoExteralSrc){
		g_print("%s, new GstVideoExteralSrc failed", __FUNCTION__);
		return -1;
	}

	m_isInited = true;

	return 0;
}