#ifndef GST_MP4_STREAMING_MANAGER_H
#define GST_MP4_STREAMING_MANAGER_H

#include "GstFunctionCallBack.h"

class GstPipelineBuilder;
class GstHttpServer;
class GstVideoExteralSrc;

typedef void(*ManagerOverCB)();
class GstMp4StreamingManager : public GstPipelineBuilderCB, public GstHttpServerCB, public GstVideoExteralSrcCB
{
public:
	GstMp4StreamingManager(ManagerOverCB cb);
	~GstMp4StreamingManager();

	int SetVideoSrc(int index);

	//scaleK : 缩放系数为 0 -2，0-1之间为缩小，1-2之间为放大
	//videoType : 0 bgr   1 yuv
	//algorithm : 0 bgr  1 hsv 2  bgr and picture  3 HSV_FULL：
	int SetMattingConfig(float scaleK, int videoType, int algorithm);

	int SetVideoConfig(int frameRate, int bitRate, int gop);

	int SetAudioName(const char* name);

	int SetFragmentDuration(int duration);

	int SetHttp(const char* ip, int port);

	int Start();

	int Stop();

	//GstPipelineBuilderCB
	void CapResolv();

	void SocketRemove(void* socket);

	void PipelineOver();

	//GstHttpServerCB
	void* GetSocketSink();

	const char* GetContentType();

	void ConnectOk() ;

	//GstVideoExteralSrcCB
	void PushVideo(int width, int height, const unsigned char* data, int size);

private:
	GstMp4StreamingManager(const GstMp4StreamingManager&);
	GstMp4StreamingManager& operator = (const GstMp4StreamingManager&);

	const char* GetSdkPath();

	int Init();

	int m_duration = 3000;
	int m_videoIndex = 0;
	int m_videoSrcFrameRate = 30;
	int m_videoDstFrameRate = 30;
	int m_videoBitRate = 1024 * 1024;
	int m_videoGop = 30;
	float m_videoMattingScaleK = 0;
	int m_videoSrcType = 0;
	int m_videoMattingAlgorithm = 0;

	char* m_audioName = nullptr;
	char* m_ip = nullptr;
	int m_port = 80;

	bool m_isInited = false;
	bool m_isStarted = false;

	ManagerOverCB			m_managerOverCB = nullptr;
	GstPipelineBuilder*		m_pipelineBuilder = nullptr;
	GstHttpServer*			m_httpServer = nullptr;
	GstVideoExteralSrc*		m_videoExteralSrc = nullptr;
};


#endif  //GST_MP4_STREAMING_MANAGER_H