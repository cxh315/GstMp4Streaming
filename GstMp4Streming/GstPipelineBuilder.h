#ifndef GST_PIPELINE_BUILDER
#define GST_PIPELINE_BUILDER

#include <gst/gst.h>
#include <gio/gio.h>

typedef struct _ElementData {
	GstElement* videoSrc;
	GstElement* audioSrc;
	GstElement* videoRate;
	GstElement* videoRateCaps;
	GstElement* videoConvert;
	GstElement* clockOverlay;
	GstElement* audioConvert;
	GstElement* audioResample;
	GstElement* videoEnc;
	GstElement* audioEnc;
	GstElement* videoParse;
	GstElement* audioParse;
	GstElement* muxer;
	GstElement* httpsink;

	_ElementData()
	{
		videoSrc = nullptr;
		audioSrc = nullptr;
		videoRate = nullptr;
		videoRateCaps = nullptr;
		videoConvert = nullptr;
		clockOverlay = nullptr;
		audioConvert = nullptr;
		audioResample = nullptr;
		videoEnc = nullptr;
		audioEnc = nullptr;
		videoParse = nullptr;
		audioParse = nullptr;
		muxer = nullptr;
		httpsink = nullptr;
	}

} ElementData;

class GstPipelineBuilderCB;
class GstPipelineBuilder
{
public:
	GstPipelineBuilder(GstPipelineBuilderCB* cb, char* path);
	~GstPipelineBuilder();

	int SetFragmentDuration(int duration); //ms

	int SetVideoConfig(const gchar* format, int width, int height, int srcFrameRate, int dstFrameRate, int bitrate, int gop);

	int SetAudioName(const char* name);

	int Init();

	int Test();

	int Release();

	int SetPlay();

	int SetStop();

	void PushVideoData(int width, int height, const guchar* data, int size);

	gpointer GetSocketSink();

	const gchar* GetContentType();

private:
	GstPipelineBuilder(const GstPipelineBuilder&);
	GstPipelineBuilder& operator = (const GstPipelineBuilder&);

	static gboolean OnMyBusCallback(GstBus *bus, GstMessage *message, gpointer data);

	gboolean MyBusCallback(GstBus *bus, GstMessage *message);

	static void OnStreamCapsChanged(GObject *obj, GParamSpec *pspec, gpointer data);

	void StreamCapsChanged(GObject *obj, GParamSpec *pspec);

	static void OnClientSocketRemoved(GstElement * element, GSocket * socket, gpointer data);

	void ClientSocketRemoved(GstElement * element, GSocket * socket);

	void StateChanged(GstMessage *msg);

	int CreatePipeline();

	int CreateElement(GstElement*& pipeline, ElementData* data, bool needAudio);

	int ConfingElement(const ElementData* data);

	int LinkElement(const GstElement* pipeline, const ElementData* data, bool needAudio);

	int SetBusMessageCB(GstElement* pipeline);

	GstState							m_state = GST_STATE_NULL;
	GMutex								m_mutex;
	GstPipelineBuilderCB*		m_callback = nullptr;
	bool									m_isInited = false;
	bool									m_isRunning = false;

	GstElement*						m_pipeline = nullptr;
	ElementData						m_pipelineData;

	gchar*								m_contentType = nullptr;
	int										m_busWatchId = 0;
	bool									m_isNeedAudio = false;
	bool									m_isAppSrc = true;

	bool									m_saveFirstFrame = false;

	int										m_fragmentDuration = 500; // ms
	gchar*								m_videoFormat = nullptr;
	int										m_width = 640;
	int										m_height = 480;
	int										m_bitrate = 1024 * 1024;
	int										m_srcFrameRate = 30;
	int										m_dstFrameRate = 30;
	int										m_gop = 30;
	gchar*								m_audioName = nullptr;
};

#endif //GST_PIPELINE_BUILDER
