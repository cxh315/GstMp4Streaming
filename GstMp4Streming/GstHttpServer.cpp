#include <string>

#include "GstFunctionCallBack.h"
#include "GstHttpServer.h"

GstHttpServer::GstHttpServer(GstHttpServerCB* cb)
: m_callback(cb)
{
	g_mutex_init(&m_mutex);
	g_mutex_init(&m_clientMutex);
	g_mutex_init(&m_capMutex);
}

GstHttpServer::~GstHttpServer()
{
	if (m_ip){
		g_free(m_ip);
		m_ip = nullptr;
	}

	g_mutex_clear(&m_mutex);
	g_mutex_clear(&m_clientMutex);
	g_mutex_clear(&m_capMutex);
}

int GstHttpServer::Start(const gchar* ip, int port)
{
	g_mutex_lock(&m_mutex);

	m_ip = g_strdup(ip);
	m_port = port;;

	if (!m_thread.joinable()){
		m_thread = std::thread(ServerThread, this);
	}

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstHttpServer::Stop()
{
	g_mutex_lock(&m_mutex);

	if (m_loop){
		g_main_loop_quit(m_loop);
	}

	if (m_thread.joinable()){
		m_thread.join();
	}

	if (m_service){
		g_socket_listener_close(G_SOCKET_LISTENER(m_service));
		g_socket_service_stop(m_service);
		g_object_unref(m_service);
		m_service = nullptr;
	}

	if (m_loop){
		g_main_loop_unref(m_loop);
		m_loop = nullptr;
	}

	g_print("%s, cancel http server \n", __FUNCTION__);

	g_mutex_unlock(&m_mutex);
	return 0;
}

void GstHttpServer::CapResolve()
{
	g_mutex_lock(&m_capMutex);

	m_capResolved = TRUE;

	g_mutex_lock(&m_clientMutex);
	for (GList *l = m_clients; l; l = l->next) {
		Client *cl = (Client *)l->data;
		if (cl->waiting_200_ok) {
			SendResponse200Ok(cl);
			cl->waiting_200_ok = FALSE;
			break;
		}
	}
	g_mutex_unlock(&m_clientMutex);

	g_mutex_unlock(&m_capMutex);
}

void GstHttpServer::SocketRemove(void* socket)
{
	Client *client = NULL;

	g_mutex_lock(&m_clientMutex);

	for (GList *l = m_clients; l; l = l->next) {
		Client *tmp = (Client *)l->data;
		if (socket == tmp->socket) {
			client = tmp;
			break;
		}
	}
	g_mutex_unlock(&m_clientMutex);

	if (client){
		if (m_callback){
			gpointer socketSink = m_callback->GetSocketSink();
 			g_signal_emit_by_name(socketSink, "remove", client->socket);
		}

		RemoveClient(client);
	}
}

unsigned int _stdcall GstHttpServer::ServerThread(void* param)
{
	GstHttpServer* server = (GstHttpServer*)param;
	server->ServerStart();

	return 0;
}

void GstHttpServer::ServerStart()
{	
	m_loop = g_main_loop_new(NULL, false);
	if (!m_loop){
		g_print("%s, g_main_loop_new failed", __FUNCTION__);
		return;
	}

	m_service = g_socket_service_new();
	if (m_service){
		g_socket_listener_add_inet_port(G_SOCKET_LISTENER(m_service), m_port, NULL, NULL);
		g_signal_connect(m_service, "incoming", G_CALLBACK(OnNewConnection), this);

		g_socket_service_start(m_service);

		g_print("listening on http://127.0.0.1:%d/\n", m_port);

		g_main_loop_run(m_loop);
	}
}

gboolean GstHttpServer::OnNewConnection(GSocketService * service, GSocketConnection * connection, GObject * source, gpointer data)
{
	GstHttpServer* server = (GstHttpServer*)data;
	if (server != nullptr){
		return server->NewConnection(service, connection, source);
	}

	return true;
}

gboolean GstHttpServer::NewConnection(GSocketService * service, GSocketConnection * connection, GObject * source)
{
	Client *client = g_slice_new0(Client);
	GSocketAddress *addr;
	GInetAddress *iaddr;
	gchar *ip;
	guint16 port;

	addr = g_socket_connection_get_remote_address(connection, NULL);
	iaddr = g_inet_socket_address_get_address(G_INET_SOCKET_ADDRESS(addr));
	port = g_inet_socket_address_get_port(G_INET_SOCKET_ADDRESS(addr));
	ip = g_inet_address_to_string(iaddr);
	client->name = g_strdup_printf("%s:%u", ip, port);
	g_free(ip);
	g_object_unref(addr);

	g_print("%s, new connection %s\n", __FUNCTION__, client->name);

	client->userData = this;
	client->waiting_200_ok = FALSE;
	client->http_version = g_strdup("");
	client->connection = (GSocketConnection*)g_object_ref(connection);
	client->socket = g_socket_connection_get_socket(connection);
	client->istream =
		g_io_stream_get_input_stream(G_IO_STREAM(client->connection));
	client->ostream =
		g_io_stream_get_output_stream(G_IO_STREAM(client->connection));
	client->current_message = g_byte_array_sized_new(1024);

	client->tosource = g_timeout_source_new_seconds(5);
	g_source_set_callback(client->tosource, (GSourceFunc)OnTimeOut, client,
		NULL);
	g_source_attach(client->tosource, NULL);

	client->isource =
		g_pollable_input_stream_create_source(G_POLLABLE_INPUT_STREAM
		(client->istream), NULL);
	g_source_set_callback(client->isource, (GSourceFunc)OnReadBytes, client,
		NULL);
	g_source_attach(client->isource, NULL);

	g_mutex_lock(&m_clientMutex);
	m_clients = g_list_prepend(m_clients, client);
	g_mutex_unlock(&m_clientMutex);

	return TRUE;
}

gboolean GstHttpServer::OnReadBytes(GPollableInputStream * stream, Client * client)
{
	GstHttpServer* server = (GstHttpServer*)client->userData;
	if (server){
		return server->ReadBytes(stream, client);
	}

	return false;
}

gboolean GstHttpServer::ReadBytes(GPollableInputStream * stream, Client * client)
{
	gssize r;
	gchar data[4096];
	GError *err = NULL;

	do {
		r = g_pollable_input_stream_read_nonblocking(G_POLLABLE_INPUT_STREAM
			(client->istream), data, sizeof(data), NULL, &err);
		if (r > 0)
			g_byte_array_append(client->current_message, (guint8 *)data, r);
	} while (r > 0);

	if (r == 0) {
		RemoveClient(client);
		return FALSE;
	}
	else if (g_error_matches(err, G_IO_ERROR, G_IO_ERROR_WOULD_BLOCK)) {
		guint8 *tmp = client->current_message->data;
		guint tmp_len = client->current_message->len;

		g_clear_error(&err);

		while (tmp_len > 3) {
			if (tmp[0] == 0x0d && tmp[1] == 0x0a && tmp[2] == 0x0d && tmp[3] == 0x0a) {
				guint len;

				g_byte_array_append(client->current_message, (const guint8 *) "\0", 1);
				len = tmp - client->current_message->data + 5;
				ClientMessage(client, (gchar *)client->current_message->data, len);
				g_byte_array_remove_range(client->current_message, 0, len);
				tmp = client->current_message->data;
				tmp_len = client->current_message->len;
			}
			else {
				tmp++;
				tmp_len--;
			}
		}

		if (client->current_message->len >= 1024 * 1024) {
			g_print("%s, no complete request after 1MB of data\n", __FUNCTION__);
			RemoveClient(client);
			return FALSE;
		}

		return TRUE;
	}
	else {
		g_print("%s, read error %s\n", __FUNCTION__, err->message);
		g_clear_error(&err);
		RemoveClient(client);
		return FALSE;
	}

	return FALSE;
}

gboolean GstHttpServer::OnTimeOut(Client * client)
{
	GstHttpServer* server = (GstHttpServer*)client->userData;
	if (server){
		return server->TimeOut(client);
	}

	return false;
}

gboolean GstHttpServer::TimeOut(Client * client)
{
	g_print("%s, timeout\n", __FUNCTION__);

	RemoveClient(client);
	return false;
}

void GstHttpServer::WriteBytes(Client * client, const gchar * data, guint len)
{
	gssize w;
	GError *err = NULL;

	/* TODO: We assume this never blocks */
	do {
		w = g_output_stream_write(client->ostream, data, len, NULL, &err);
		if (w > 0) {
			len -= w;
			data += w;
		}
	} while (w > 0 && len > 0);

	if (w <= 0) {
		if (err) {
			g_print("Write error %s\n", err->message);
			g_clear_error(&err);
		}
		RemoveClient(client);
	}
}

void GstHttpServer::ClientMessage(Client * client, const gchar * data, guint len)
{
	gboolean http_head_request = FALSE;
	gboolean http_get_request = FALSE;
	gchar **lines = g_strsplit_set(data, "\r\n", -1);

	if (g_str_has_prefix(lines[0], "HEAD"))
		http_head_request = TRUE;
	else if (g_str_has_prefix(lines[0], "GET"))
		http_get_request = TRUE;

	if (http_head_request || http_get_request) {
		gchar **parts = g_strsplit(lines[0], " ", -1);
		gboolean ok = FALSE;

		g_free(client->http_version);

		if (parts[1] && parts[2] && *parts[2] != '\0')
			client->http_version = g_strdup(parts[2]);
		else
			client->http_version = g_strdup("HTTP/1.0");

		if (parts[1] && strcmp(parts[1], "/") == 0) {
			g_mutex_lock(&m_capMutex);
			if (m_capResolved)
				SendResponse200Ok(client);
			else
				client->waiting_200_ok = TRUE;
			g_mutex_unlock(&m_capMutex);
			ok = TRUE;
		}
		else {
			SendResponse404NotFound(client);
		}
		g_strfreev(parts);

		if (ok) {
			if (http_get_request) {
				/* Start streaming to client socket */
				g_source_destroy(client->isource);
				g_source_unref(client->isource);
				client->isource = NULL;
				g_source_destroy(client->tosource);
				g_source_unref(client->tosource);
				client->tosource = NULL;
				g_print("%s, starting to stream to %s\n", __FUNCTION__, client->name);
				if (m_callback){
					gpointer socketSink = m_callback->GetSocketSink();
					g_signal_emit_by_name(socketSink, "add", client->socket);
				}else{
					g_print("%s, http server call back is null", __FUNCTION__);
				}
			}

			if (m_callback){
				m_callback->ConnectOk();
			}
		}
	}
	else {
		gchar **parts = g_strsplit(lines[0], " ", -1);
		gchar *response;
		const gchar *http_version;

		if (parts[1] && parts[2] && *parts[2] != '\0')
			http_version = parts[2];
		else
			http_version = "HTTP/1.0";

		response = g_strdup_printf("%s 400 Bad Request\r\n\r\n", http_version);
		WriteBytes(client, response, strlen(response));
		g_free(response);
		g_strfreev(parts);
		RemoveClient(client);
	}

	g_strfreev(lines);
}

void GstHttpServer::RemoveClient(Client * client)
{
	g_print("%s, removing connection %s\n", __FUNCTION__, client->name);

	g_mutex_lock(&m_clientMutex);
	m_clients = g_list_remove(m_clients, client);
	g_mutex_unlock(&m_clientMutex);

	g_free(client->name);
	g_free(client->http_version);

	if (client->isource) {
		g_source_destroy(client->isource);
		g_source_unref(client->isource);
	}
	if (client->tosource) {
		g_source_destroy(client->tosource);
		g_source_unref(client->tosource);
	}
	g_object_unref(client->connection);
	g_byte_array_unref(client->current_message);

	g_slice_free(Client, client);
}

void GstHttpServer::SendResponse200Ok(Client * client)
{
	gchar *response;
	const gchar* content_type = "i don't konw content type too";
	if (m_callback){
		content_type = m_callback->GetContentType();
	}

	response = g_strdup_printf("%s 200 OK\r\n%s\r\n", client->http_version,content_type);
	WriteBytes(client, response, strlen(response));
	g_free(response);
}

void GstHttpServer::SendResponse404NotFound(Client * client)
{
	gchar *response;
	response = g_strdup_printf("%s 404 Not Found\r\n\r\n", client->http_version);
	WriteBytes(client, response, strlen(response));
	g_free(response);
}