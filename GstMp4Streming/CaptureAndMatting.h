#pragma once

//#include<iostream>
#include<windows.h>
#include<stdio.h>
#include<string.h>
#include <map>
#include <opencv2/opencv.hpp>

#include "QCAP.H"
#include "../Mp4StreamingTest/VideoData.h"

using namespace cv;


#define MAX_CHANNEL_NUM 12
#define  RETURN_FAIL 0
#define  RETURN_OK 1
#define RAWIMAGERESOLUTION 1920*1080

void matting_data(VideoData vd);
typedef void (*MattingDataCallBack)(int width, int height, const unsigned char* data, int size, void* userData);
//对外接口
void SetCameraIndex(int index);// index 的范围为0 - 11 共12个摄像头
void SetRawImageDataScaleK(float k);//原始摄像头每帧的图片数据为1920*1080  通过设置该系数对元素图片进行缩放   k的取值范围是 0 - 2
void SetMattingAlgorithm(int whichAlogrithm);// 暂时有4种抠图方法  取值为 0 1 2 3
void SetBGRorYUV(int index);// 0 代表RGB  1代表YUV
void SetMattingDataCallBack(MattingDataCallBack cb, void* userData);
unsigned char* GetMattingData_RGB();
unsigned char* GetMattingData_YUV42O();
int GetImageWidth();
int GetImageHight();
void GetMattingData_RGB(unsigned char* &buffer, int length);// length = GetImageWidth()*GetImageHight()*3;
void GetMattingData_YUV(unsigned char* &buffer, int length); //length = GetImageWidth()*GetImageHight() * 3/2;
void Destroy(); // 退出应用时关闭设备，释放内存。

void Matting_Data();


//不需要调用外部调用的
void ReleseMemory();
void Stop();
void GetMattingData_RGB(unsigned char* &mattingData, long length);
void GetMattingData_HSV(unsigned char* &mattingData, long length);
void yuv420_to_HSV_FULL_mat(Mat &dst, unsigned char* pYUV420, int width, int height);
void Matting_HSV_FULL_mat(Mat picture);
void   show_information();
int    init_capture_card_func();
int    set_video_input_func(struct command_s command_data);

//回调函数
QRETURN on_process_format_changed_func(PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, ULONG nVideoWidth, ULONG nVideoHeight,
	BOOL bVideoIsInterleaved, double dVideoFrameRate, ULONG nAudioChannels, ULONG nAudioBitsPerSample,
	ULONG nAudioSampleFrequency, PVOID pUserData);

QRETURN on_process_no_signal_detected_func(PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, PVOID pUserData);

QRETURN on_process_signal_removed_func(PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, PVOID pUserData);

QRETURN on_process_preview_video_buffer(PVOID pDevice, double dSampleTime, BYTE* pFrameBuffer, ULONG FrameBufferLen, PVOID pUserData);

void yuv420_to_mat(Mat &dst, unsigned char* pYUV420, int width, int height);

bool Matting_mat(Mat picture);

void yuv420_to_HSV_mat(Mat &dst, unsigned char* pYUV420, int width, int height);

void Matting_HSV_mat(Mat picture);




