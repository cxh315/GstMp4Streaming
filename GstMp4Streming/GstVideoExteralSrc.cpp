#include <windows.h>
#include <iostream>
#include <ctime>
#include "CaptureAndMatting.h"
#include "GstFunctionCallBack.h"
#include "GstVideoExteralSrc.h"


GstVideoExteralSrc::GstVideoExteralSrc(GstVideoExteralSrcCB* cb)
: m_callback(cb)
{
	g_mutex_init(&m_mutex);
}

GstVideoExteralSrc::~GstVideoExteralSrc()
{
	g_mutex_clear(&m_mutex);
}

int GstVideoExteralSrc::SetVideoSrc(int index)
{
	g_mutex_lock(&m_mutex);

	if (m_isStarted)
	{
		SetCameraIndex(index);
	}

	m_videoIndex = index;

	printf("%s, set video src index %d \n", __FUNCTION__, index);

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstVideoExteralSrc::SetMattingConfig(float scaleK, int videoType, int algorithm)
{
	g_mutex_lock(&m_mutex);

	if (m_isStarted){
		g_mutex_unlock(&m_mutex);
		return -1;
	}

	m_videoMattingScaleK = scaleK;
	m_videoType = videoType;
	m_videoMattingAlgorithm = algorithm;

	printf("%s, set video config: scalek %f, videoType %d, algorithm %d", __FUNCTION__, scaleK, videoType, algorithm);

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstVideoExteralSrc::GetVideoConfig(int& width, int& height, char*& colorspace)
{
	g_mutex_lock(&m_mutex);

	int ret = -1;
	if (m_isStarted)
	{
		if (m_videoType == 0 )
		{
			colorspace = "BGR";
		}
		else if (m_videoType == 1)
		{
			colorspace = "I420";
		}
		else
		{
			colorspace = "UNKONW";
		}

		width = GetImageWidth();
		height = GetImageHight();
		//printf("%s, try get width %d, height %d \n", __FUNCTION__, width, height);
		if (width > 0 && height > 0)
		{
			ret = 0;
		}
	}

	g_mutex_unlock(&m_mutex);
	return ret;
}

int GstVideoExteralSrc::Start()
{
	g_mutex_lock(&m_mutex);

	if (!m_isStarted)
	{
		SetMattingDataCallBack(MattingDataCallBack, this);
		SetRawImageDataScaleK(m_videoMattingScaleK);
		SetMattingAlgorithm(m_videoMattingAlgorithm);
		SetCameraIndex(m_videoIndex);
		SetBGRorYUV(m_videoType);
		init_capture_card_func();

		m_isStarted = true;
	}

	g_mutex_unlock(&m_mutex);
	return 0;
}

int GstVideoExteralSrc::Stop()
{
	g_mutex_lock(&m_mutex);

	printf("%s, video capture begin to stop", __FUNCTION__);

	if (m_isStarted)
	{
		m_isStarted = false;
		Destroy();
	}
	printf("%s, video capture end to stop", __FUNCTION__);

	g_mutex_unlock(&m_mutex);
	return 0;
}

void GstVideoExteralSrc::MattingDataCallBack(int width, int height, const unsigned char* data, int size, void* userData)
{
	GstVideoExteralSrc* exteraSrc = (GstVideoExteralSrc*)userData;
	if (exteraSrc){
		exteraSrc->PushData(width, height, data, size);
	}
}

void GstVideoExteralSrc::PushData(int width, int height, const unsigned char* data, int size)
{
	if (m_callback && m_isStarted){
		m_callback->PushVideo(width, height, data, size);
	}
}