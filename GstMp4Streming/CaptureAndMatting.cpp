#include "CaptureAndMatting.h"
#include "../Mp4StreamingTest/queue_safe.h"
//#include "VideoData.h"
#include "../Mp4StreamingTest/VideoData.h"
//#include "vld.h"

extern gdface::mt::threadsafe_queue<VideoData> queuetestData;

typedef struct mattingPictureData
{
	cv::Size dsize;
	cv::Size dsizeBackground;
	cv::Mat m_scaleImage;
	cv::Mat m_scaleBackgroundImage;
	double m_scale = 0.5f;
	int  m_col;
	int  m_row;
	Mat backgroundImg;
}MattingPictureData;

typedef struct CaputreStatus_s
{
	BOOL  m_bIsRecord_flag;
	BOOL  m_bNoSignal_flag;
	BOOL  m_Deinterlace_flag;
	BOOL  m_ShareRecord_flag;
	bool  m_bIsHaveSignal_flag;
}CaputreStatus_t;



struct CaptureCard
{
	PVOID  m_hVideoDevice;
	ULONG  m_nVideoWidth;
	ULONG  m_nVideoHeight;
	ULONG  m_nVideoInput;
	ULONG  m_nAudioInput;
	double  m_dVideoFrameRate;
	// Status 
	CaputreStatus_t m_status;
	QRESULT m_errno;
	int m_index;
};

typedef struct command{
	int whichCameraDataToMatting;
	float rawImageScale;
	BOOL execute_status;
}Command;



CaptureCard m_capture_card[MAX_CHANNEL_NUM];
MattingPictureData m_matting_Data;
int camreaIndex[MAX_CHANNEL_NUM];
int whichCameraToMatting;
int whichMattingAlgorithm = 0;
unsigned char* mattingDataForBGR;
unsigned char* mattingDataForYUV;
int bgrORYUVoutput = 0;

Mat mattingDataMat;
Mat mattingDataMatYUV;
MattingDataCallBack mattingDataCb = nullptr;
void* mattingDataCbUserData = nullptr;


void Matting_Data()
{
	
}


void yuv420_to_mat(Mat* dst, unsigned char* pYUV420, int width, int height)
{
	if (!pYUV420) {
		return;
	}
	IplImage *yuvimage, *rgbimg, *yimg, *uimg, *vimg, *uuimg, *vvimg;
	int nWidth = width;
	int nHeight = height;
	rgbimg = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);
	yuvimage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);
	yimg = cvCreateImageHeader(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	uimg = cvCreateImageHeader(cvSize(nWidth / 2, nHeight / 2), IPL_DEPTH_8U, 1);
	vimg = cvCreateImageHeader(cvSize(nWidth / 2, nHeight / 2), IPL_DEPTH_8U, 1);
	uuimg = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	vvimg = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	cvSetData(yimg, pYUV420, nWidth);
	cvSetData(uimg, pYUV420 + nWidth*nHeight, nWidth / 2);
	cvSetData(vimg, pYUV420 + long(nWidth*nHeight*1.25), nWidth / 2);
	cvResize(uimg, uuimg, CV_INTER_LINEAR);
	cvResize(vimg, vvimg, CV_INTER_LINEAR);
	cvMerge(yimg, uuimg, vvimg, NULL, yuvimage);
	cvCvtColor(yuvimage, rgbimg, CV_YCrCb2BGR);

	cvReleaseImage(&uuimg);
	cvReleaseImage(&vvimg);
	cvReleaseImageHeader(&yimg);
	cvReleaseImageHeader(&uimg);
	cvReleaseImageHeader(&vimg);
	cvReleaseImage(&yuvimage);

	*dst = Mat(rgbimg, true).clone();

	cvReleaseImage(&rgbimg);
}

void yuv420_to_mat(Mat &dst, unsigned char* pYUV420, int width, int height)
{
	if (!pYUV420) {
		return;
	}
	IplImage *yuvimage, *rgbimg, *yimg, *uimg, *vimg, *uuimg, *vvimg;
	int nWidth = width;
	int nHeight = height;
	rgbimg = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);
	yuvimage = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);
	yimg = cvCreateImageHeader(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	uimg = cvCreateImageHeader(cvSize(nWidth / 2, nHeight / 2), IPL_DEPTH_8U, 1);
	vimg = cvCreateImageHeader(cvSize(nWidth / 2, nHeight / 2), IPL_DEPTH_8U, 1);
	uuimg = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	vvimg = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 1);
	cvSetData(yimg, pYUV420, nWidth);
	cvSetData(uimg, pYUV420 + nWidth*nHeight, nWidth / 2);
	cvSetData(vimg, pYUV420 + long(nWidth*nHeight*1.25), nWidth / 2);
	cvResize(uimg, uuimg, CV_INTER_LINEAR);
	cvResize(vimg, vvimg, CV_INTER_LINEAR);
	cvMerge(yimg, uuimg, vvimg, NULL, yuvimage);
	cvCvtColor(yuvimage, rgbimg, CV_YCrCb2BGR);

	cvReleaseImage(&uuimg);
	cvReleaseImage(&vvimg);
	cvReleaseImageHeader(&yimg);
	cvReleaseImageHeader(&uimg);
	cvReleaseImageHeader(&vimg);
	cvReleaseImage(&yuvimage);
	//dst = Mat(rgbimg, true);
	dst = Mat(rgbimg, true).clone();
	cvReleaseImage(&rgbimg);
}



bool Matting_mat(Mat picture)
{
	static bool isFirstRun = true;
	if (isFirstRun)
	{
		/*m_matting_Data.dsize = cv::Size(picture.cols*m_matting_Data.m_scale, picture.rows*m_matting_Data.m_scale);
		m_matting_Data.m_scaleImage = cv::Mat(m_matting_Data.dsize, CV_32S);
		cv::resize(picture, m_matting_Data.m_scaleImage, m_matting_Data.dsize);*/

		m_matting_Data.m_col = m_matting_Data.m_scaleImage.cols;
		m_matting_Data.m_row = m_matting_Data.m_scaleImage.rows;

		if (m_matting_Data.m_scaleImage.isContinuous())
		{
			m_matting_Data.m_col *= m_matting_Data.m_row;
			m_matting_Data.m_row = 1;
		}
		isFirstRun = false;
	}
	//#pragma omp parallel for 
	for (int k = 0; k < m_matting_Data.m_row; k++)  //行循环
	{
		cv::Vec3b* data = m_matting_Data.m_scaleImage.ptr<cv::Vec3b>(k);  //获取第i行的首地址
		for (int j = 0; j < m_matting_Data.m_col; j++)   //列循环,3列为一个像素
		{
			if (/*((data[j][1] - data[j][0]>25))
				&&*/((data[j][1] - data[j][2]>5)))
				data[j] = cv::Vec3b(0, 0, 0);
			//data[j] = cv::Vec3b(255, 255, 255);
		}
	}
	return true;
	//imshow("BGRMatting", m_matting_Data.m_scaleImage);
	//cv::waitKey(1);
}



void cameraPicture_mat(Mat picture)
{
	//m_matting_Data.dsize = cv::Size(picture.cols*m_matting_Data.m_scale, picture.rows*m_matting_Data.m_scale);
	static bool isFirstRun = true;
	static cv::Size image_size;

	
	/*imshow("srcImage", picture);
	cv::waitKey(10);*/
	
	if (isFirstRun)
	{
		image_size = cv::Size(picture.cols*m_matting_Data.m_scale, picture.rows*m_matting_Data.m_scale);
		m_matting_Data.m_scaleImage = cv::Mat(image_size, CV_32S);
		
		isFirstRun = false;
	}
	try{
		cv::resize(picture, m_matting_Data.m_scaleImage, image_size, CV_INTER_CUBIC);
	}
	catch (std::exception & c){
		std::cout << c.what() << std::endl;
	}
	catch (...){
		std::cout << "catch  matting_data_error" << std::endl;
	}

	////static bool isFirst = true;
	//imshow("scaleImage", m_matting_Data.m_scaleImage);
	//cv::waitKey(10);
}

void MattingAndBackground_mat(Mat picture, const char* pictureName)
{
	if (m_matting_Data.backgroundImg.empty())
	{
		m_matting_Data.backgroundImg = imread(pictureName);
	}
	m_matting_Data.dsizeBackground = cv::Size(m_matting_Data.backgroundImg.cols*m_matting_Data.m_scale, m_matting_Data.backgroundImg.rows*m_matting_Data.m_scale);
	m_matting_Data.m_scaleBackgroundImage = cv::Mat(m_matting_Data.dsizeBackground, CV_32S);
	cv::resize(m_matting_Data.backgroundImg, m_matting_Data.m_scaleBackgroundImage, m_matting_Data.dsizeBackground);
	m_matting_Data.dsize = cv::Size(picture.cols*m_matting_Data.m_scale, picture.rows*m_matting_Data.m_scale);
	m_matting_Data.m_scaleImage = cv::Mat(m_matting_Data.dsize, CV_32S);
	cv::resize(picture, m_matting_Data.m_scaleImage, m_matting_Data.dsize);

	static bool isFirst = true;

	if (isFirst)
	{
		m_matting_Data.m_col = m_matting_Data.m_scaleImage.cols;
		m_matting_Data.m_row = m_matting_Data.m_scaleImage.rows;
		if (m_matting_Data.m_scaleImage.isContinuous())
		{
			m_matting_Data.m_col *= m_matting_Data.m_row;
			m_matting_Data.m_row = 1;
		}
		isFirst = false;
	}
#pragma omp parallel for 
	for (int k = 0; k < m_matting_Data.m_row; k++)  //行循环
	{
		cv::Vec3b* data = m_matting_Data.m_scaleImage.ptr<cv::Vec3b>(k);  //获取第i行的首地址
		cv::Vec3b* dataBackground = m_matting_Data.m_scaleBackgroundImage.ptr<cv::Vec3b>(k);  //获取第i行的首地址
		for (int j = 0; j < m_matting_Data.m_col; j++)   //列循环,3列为一个像素
		{

			if (/*((data[j][1] - data[j][0]>15))
				&&*/ ((data[j][1] - data[j][2]>10)))
				data[j] = dataBackground[j];
		}
	}
	//imshow("MatBackgroundImage", m_matting_Data.m_scaleImage);
	//cv::waitKey(1);
}



int init_capture_card_func()
{
	for (int ch_idx = 0; ch_idx < MAX_CHANNEL_NUM; ch_idx++){
		m_capture_card[ch_idx].m_hVideoDevice = NULL;
		m_capture_card[ch_idx].m_nVideoWidth = 0;
		m_capture_card[ch_idx].m_nVideoHeight = 0;
		m_capture_card[ch_idx].m_nVideoInput = 0;
		m_capture_card[ch_idx].m_nAudioInput = 0;
		m_capture_card[ch_idx].m_dVideoFrameRate = 0;
		m_capture_card[ch_idx].m_status.m_bIsRecord_flag = FALSE;
		m_capture_card[ch_idx].m_status.m_bNoSignal_flag = TRUE;
		m_capture_card[ch_idx].m_status.m_ShareRecord_flag = TRUE;
		m_capture_card[ch_idx].m_errno = QCAP_CREATE("QP0203 PCI", ch_idx, NULL, &m_capture_card[ch_idx].m_hVideoDevice, 1);
		if (m_capture_card[ch_idx].m_errno != 0){ continue; }
		printf(" create Ch%d \n", ch_idx + 1);
		// Set Callback func		
		QCAP_REGISTER_FORMAT_CHANGED_CALLBACK(m_capture_card[ch_idx].m_hVideoDevice, on_process_format_changed_func, (PVOID)&m_capture_card[ch_idx]);
		QCAP_REGISTER_VIDEO_PREVIEW_CALLBACK(m_capture_card[ch_idx].m_hVideoDevice, on_process_preview_video_buffer, (PVOID)&m_capture_card[ch_idx]);
		QCAP_REGISTER_NO_SIGNAL_DETECTED_CALLBACK(m_capture_card[ch_idx].m_hVideoDevice, on_process_no_signal_detected_func, (PVOID)&m_capture_card[ch_idx]);
		QCAP_REGISTER_SIGNAL_REMOVED_CALLBACK(m_capture_card[ch_idx].m_hVideoDevice, on_process_signal_removed_func, (PVOID)&m_capture_card[ch_idx]);
		//QCAP_SET_VIDEO_INPUT(m_capture_card[ch_idx].m_hVideoDevice, QCAP_INPUT_TYPE_AUTO);
		m_capture_card[ch_idx].m_errno = QCAP_RUN(m_capture_card[ch_idx].m_hVideoDevice);
	}
	return RETURN_OK;
}


void SetCameraIndex(int index)
{
	if (index < 0 || index > 12)
	{
		printf(" camera index is out of the range \n");
	}
	else
		whichCameraToMatting = index;
}

void SetMattingAlgorithm(int whichAlogrithm)
{
	whichMattingAlgorithm = whichAlogrithm;
}


void SetBGRorYUV(int index)
{
	bgrORYUVoutput = index;
}

unsigned char* GetMattingData_RGB()
{

	return mattingDataForBGR;
}

void GetMattingData_RGB(unsigned char* &buffer, int length)
{
	static bool isFirstMallocMemory = true;
	if (isFirstMallocMemory)
	{
		buffer = (unsigned char*)malloc(length);
	}
	if (bgrORYUVoutput == 0)
	{
		buffer = mattingDataMat.data;
	}

}

void GetMattingData_YUV(unsigned char* &buffer, int length)
{
	static bool isFirstMallocMemory = true;
	if (isFirstMallocMemory)
	{
		buffer = (unsigned char*)malloc(length);
	}
	if (bgrORYUVoutput == 1)
	{
		buffer = mattingDataMat.data;
	}


}

int GetImageWidth()
{
	//return m_matting_Data.m_scale*m_matting_Data.m_row;
	return m_matting_Data.m_scaleImage.cols;
}

int GetImageHight()
{
	//return m_matting_Data.m_scale*m_matting_Data.m_col;
	return m_matting_Data.m_scaleImage.rows;
}

unsigned char* GetMattingData_YUV42O()
{
	return mattingDataForYUV;
}

void ReleseMemory()
{
	if (bgrORYUVoutput == 0)
	{
		free(mattingDataForBGR);
		mattingDataForBGR = NULL;
	}
	if (bgrORYUVoutput == 1)
	{
		free(mattingDataForYUV);
		mattingDataForYUV = NULL;
	}
}

void SetRawImageDataScaleK(float k)
{
	if (k > 2)
	{
		m_matting_Data.m_scale = 2;
	}
	if (k < 0)
	{
		m_matting_Data.m_scale = 1;
	}
	else
	{
		m_matting_Data.m_scale = k;
	}
}

void SetMattingDataCallBack(MattingDataCallBack cb, void* userData)
{
	mattingDataCb = cb;
	mattingDataCbUserData = userData;
}



void matting_data(VideoData vd)
{

	yuv420_to_mat(mattingDataMat,vd.data,1920,1080);
	if (mattingDataMat.empty())
	{
		std::cout << "mattingDataMat empty" << std::endl;
		return ;
	}
	cameraPicture_mat(mattingDataMat);

	Matting_mat(mattingDataMat);
	cv::cvtColor(m_matting_Data.m_scaleImage, mattingDataMatYUV, CV_BGR2YUV_I420);
	mattingDataForYUV = mattingDataMatYUV.data;


	if (mattingDataCb && mattingDataCbUserData){
		mattingDataCb(m_matting_Data.m_scaleImage.cols, m_matting_Data.m_scaleImage.rows, mattingDataForYUV, m_matting_Data.m_scale*m_matting_Data.m_scale*RAWIMAGERESOLUTION * 3 / 2, mattingDataCbUserData);
	}

}

QRETURN on_process_preview_video_buffer(PVOID pDevice, double dSampleTime, BYTE* pFrameBuffer, ULONG FrameBufferLen, PVOID pUserData)
{
	CaptureCard *m_Cap = (CaptureCard*)pUserData;
	//static bool isDown = true;
	static int timeStep = 0;
	timeStep++;

	if (timeStep == 100000)
	{
		timeStep = 1;
	}

	//VideoData*m_videoData1 = videodata_new(100);


	if (m_Cap->m_status.m_ShareRecord_flag == TRUE && FrameBufferLen !=0) {
		QRESULT ret = QCAP_SET_VIDEO_SHARE_RECORD_UNCOMPRESSION_BUFFER(whichCameraToMatting, QCAP_COLORSPACE_TYEP_YV12, m_Cap->m_nVideoWidth, m_Cap->m_nVideoHeight, pFrameBuffer, FrameBufferLen);
		if ((timeStep / 2 == 0)  || pFrameBuffer != NULL && m_Cap->m_hVideoDevice == m_capture_card[whichCameraToMatting].m_hVideoDevice)
		{

		
			VideoData* m_videoData = videodata_new(FrameBufferLen);
			
			//printf("1)===>push data...\n");
			videodata_copy(m_videoData, pFrameBuffer, FrameBufferLen);
			queuetestData.push(*m_videoData);



				/*yuv420_to_mat(mattingDataMat, pFrameBuffer, m_Cap->m_nVideoWidth, m_Cap->m_nVideoHeight);
				if (mattingDataMat.empty())
				{
					std::cout << "mattingDataMat empty" << std::endl;
					return QCAP_RT_SKIP_DISPLAY;

				}
				cameraPicture_mat(mattingDataMat);

				Matting_mat(mattingDataMat);
				cv::cvtColor(m_matting_Data.m_scaleImage, mattingDataMatYUV, CV_BGR2YUV_I420);
				mattingDataForYUV = mattingDataMatYUV.data;


			if (mattingDataCb && mattingDataCbUserData){
				mattingDataCb(m_matting_Data.m_scaleImage.cols, m_matting_Data.m_scaleImage.rows, mattingDataForYUV, m_matting_Data.m_scale*m_matting_Data.m_scale*RAWIMAGERESOLUTION * 3 / 2, mattingDataCbUserData);
			}
*/

		}
		else
		{
			return QCAP_RT_SKIP_DISPLAY;
		}
	}
	else{
		return QCAP_RT_SKIP_DISPLAY;
	}
	
	return QCAP_RT_OK;
}


QRETURN on_process_signal_removed_func(PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, PVOID pUserData)
{
	CaptureCard *m_Cap = (CaptureCard*)pUserData;
	m_Cap->m_nVideoWidth = 0;
	m_Cap->m_nVideoHeight = 0;
	m_Cap->m_status.m_bNoSignal_flag = TRUE;
	return QCAP_RT_OK;
}



QRETURN on_process_no_signal_detected_func(PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, PVOID pUserData)
{
	CaptureCard *m_Cap = (CaptureCard*)pUserData;
	m_Cap->m_nVideoWidth = 0;
	m_Cap->m_nVideoHeight = 0;
	m_Cap->m_status.m_bNoSignal_flag = TRUE;
	return QCAP_RT_OK;
}


void Stop()
{
	//destroyWindow("test");
	//destroyWindow("srcImage");
}

void Destroy()
{
	for (int ch_idx = 0; ch_idx < MAX_CHANNEL_NUM; ch_idx++) {

		if (m_capture_card[ch_idx].m_hVideoDevice != NULL)
		{
			if (m_capture_card[ch_idx].m_status.m_bIsRecord_flag == FALSE) {
				QCAP_STOP_RECORD(m_capture_card[ch_idx].m_hVideoDevice, 0);
			}

			QCAP_STOP(m_capture_card[ch_idx].m_hVideoDevice);
			QCAP_DESTROY(m_capture_card[ch_idx].m_hVideoDevice);
			m_capture_card[ch_idx].m_hVideoDevice = NULL;
		}
	}

	//ReleseMemory();
	return;

}

QRETURN on_process_format_changed_func(PVOID pDevice, ULONG nVideoInput, ULONG nAudioInput, ULONG nVideoWidth, ULONG nVideoHeight,
	BOOL bVideoIsInterleaved, double dVideoFrameRate, ULONG nAudioChannels, ULONG nAudioBitsPerSample,
	ULONG nAudioSampleFrequency, PVOID pUserData)
{
	CaptureCard *m_Cap = (CaptureCard*)pUserData;
	m_Cap->m_nVideoInput = nVideoInput;
	m_Cap->m_nAudioInput = nAudioInput;
	m_Cap->m_nVideoWidth = nVideoWidth;
	m_Cap->m_nVideoHeight = nVideoHeight;
	m_Cap->m_dVideoFrameRate = dVideoFrameRate;


	if (nVideoInput == 0 && nVideoHeight == 0 && dVideoFrameRate == 0.0 && nAudioChannels == 0) {
		m_Cap->m_status.m_bNoSignal_flag = TRUE;
	}
	else {
		m_Cap->m_status.m_bNoSignal_flag = FALSE;
	}

	return QCAP_RT_OK;
}




